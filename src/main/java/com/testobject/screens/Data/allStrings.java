package com.testobject.screens.Data;

public class allStrings {

    public static final String SUGGESTED = "POLECANE";

    //vacation types Strings

    public static final String AIR_VACATION = "Wakacje samolotem";
    public static final String CAR_VACATION = "Wakacje samochodem";
    public static final String WALKING_TOUR_VACATION = "Wycieczki objazdowe";
    public static final String SKI_VACATION = "Narty";
    public static final String CHARTER_TICKETS_VACATION = "Bilety czarterowe";

    //vacation days Strings

    public static final String THREE_FIVE_DAYS = "3-5 dni";
    public static final String SIX_EIGHT_DAYS = "6-8 dni";
    public static final String NINE_TWELVE_DAYS = "9-12 dni";
    public static final String THIRTEEN_FIFTEEN_DAYS = "13-15 dni";
    public static final String CUSTOM_9_DAYS = "9 dni";

    //passengers

    public static final String FOUR_GROWNUPS_0_CHILDREN = "4 dorosłych, 0 dzieci";
    public static final String TWO_GROWNUPS_1_CHILDREN = "2 dorosłych, 1 dziecko";
    public static final String THREE_GROWNUPS_2_CHILDREN = "3 dorosłych, 2 dzieci";
    public static final String THREE_GROWNUPS_0_CHILDREN = "3 dorosłych, 0 dzieci";

    //directions

    public static final String ALL_OF_DIRECTIONS = "Wszystkie kierunki";
    public static final String ANY_OF_DIRECTION = "Dowolny kierunek";

    // Countries and Cities

    public static final String BALI = "Bali";
    public static final String BULGARIAN_RIVIERA = "Riwiera Bułgarska";
    public static final String BULGARIAN_BURGAS = "Burgas";
    public static final String CRETE = "Kreta";
    public static final String CRETE_CHARTER = "Kreta (CHQ_CTR)";
    public static final String CYPR = "Cypr";
    public static final String EGIPT = "Egipt";
    public static final String GREECE = "Grecja";
    public static final String DOMINICAN_REPUBLIC = "Dominikana";
    public static final String MALDIVES = "Malediwy";
    public static final String MONTENEGRO = "Czarnogóra";
    public static final String MAURITIUS = "Mauritius";
    public static final String SEYCHELLES = "Seszele";
    public static final String SPAIN = "Hiszpania";
    public static final String KENYA = "Kenia";
    public static final String MADEIRA = "Madera";
    public static final String CROATIA = "Chorwacja";

    //directions

    public static final String UP = "u";
    public static final String DOWN = "d";
    public static final String LEFT = "l";
    public static final String RIGHT = "r";

    // Contacts

    public static final String CONTACT_NUMBER = "+48 22 255 03 49";
    public static final String CONTACT_HEADER_TEXT = "Zarezerwuj swoje beztroskie wakacje lub dowiedz się więcej o naszej ofercie";
    public static final String CONTACT_POPUP_INFO_ONE = "7 dni w tygodniu";
    public static final String CONTACT_POPUP_INFO_TWO = "Minimum formalności";
    public static final String CONTACT_POPUP_INFO_THREE_ANDROID = "Profesjonalni doradcy,\n" +
            "pasjonaci turystyki";
    public static final String CONTACT_POPUP_INFO_THREE_IOS = "Profesjonalni doradcy, pasjonaci turystyki";
    public static final String CONTACT_POPUP_CALL_BUTTON= "ZADZWOŃ TERAZ";
    public static final String CONTACT_POPUP_OPENING_HOURS_1_ANDROID= "pn-pt: \\d\\d:\\d\\d-\\d\\d:\\d\\d";
    public static final String CONTACT_POPUP_OPENING_HOURS_2_ANDROID= "sb-nd: \\d\\d:\\d\\d-\\d\\d:\\d\\d";
    public static final String CONTACT_POPUP_OPENING_HOURS_1_IOS= "pn-pt: sb-nd:";
    public static final String CONTACT_POPUP_OPENING_HOURS_2_IOS= "\\d\\d:\\d\\d-\\d\\d:\\d\\d \\d\\d:\\d\\d-\\d\\d:\\d\\d";

    // More
    public static final String APPLICATION = "Aplikacja";
    public static final String SETTINGS = "Ustawienia aplikacji";
    public static final String DASH = "-";
    public static final String VACATION_DATES = "Dowolny";

    //Cities for sorting

    public static final String BYDGOSZCZ = "Bydgoszcz";
    public static final String GDANSK = "Gdańsk";
    public static final String KATOWICE = "Katowice";
    public static final String KRAKOW = "Kraków";
    public static final String POZNAN = "Poznań";
    public static final String RZESZOW = "Rzeszów";

    //favourites

    public static final String NO_FAVOURITES_ANDROID= "Aktualnie Twoja\n" +
            "lista ulubionych ofert jest pusta.\n" +
            "Aby dodać ofertę do ulubionych\n" +
            "oznacz ją symbolem";

    public static final String NO_FAVOURITES_IOS= "Aktualnie Twoja lista ulubionych *";


    //boolean strings

    public static final String TRUE = "true";
    public static final String FALSE = "false";

    //numbers

    public static final String ONE = "1";
    public static final String TWO = "2";
    public static final String TWO_PLUS_ONE = "2+1";
    public static final String THREE = "3";
    public static final String THREE_PLUS_TWO = "3+2";
    public static final String FOUR = "4";
    public static final String FIVE = "5";
    public static final String SIX = "6";
    public static final String NINE = "9";

    //airports

    public static final String TWO_AIRPORTS = "2 lotniska";
    public static final String THREE_AIRPORTS = "3 lotniska";

}
