package com.testobject.screens.FavouritesScreen;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.NoSuchElementException;

public class FavouritesScreen extends AbstractScreen {

    public FavouritesScreen(AppiumDriver driver) {
        super(driver);
    }

    private FavouritesScreenUIElements uiObject = new FavouritesScreenUIElements(driver);

    //static methods

    public FavouritesScreen clickFirstRedHeart() {
        uiObject.firstRedHeartButton.click();
        return this;
    }

    public FavouritesScreen clickSecondRedHeart() {
        uiObject.secondRedHeartButton.click();
        return this;
    }

    public void clickFirstOffer() {
        uiObject.firstOfferItem.click();
    }

    public String getPlaceholderText() {
        return uiObject.placeholderLabel.getText();
    }

    public boolean thereIsNoFirstFavouriteOffer() {

        try {
            return !uiObject.firstRedHeartButton.isDisplayed();
        } catch(NoSuchElementException ne) {
            return true;
        }
    }

    public int getAmountOfFavourites() {
        return Integer.parseInt(uiObject.favCounter.getText());
    }

    public String getAmountStringOfFavouritesForIOS() {
        return uiObject.favCounterLabel.getText();
    }

    public String getHeartColor() {
        return uiObject.firstRedHeartButton.getCssValue("background-color");
    }

    public String getOfferTitle() {
        return uiObject.offerName.getText();
    }

}
