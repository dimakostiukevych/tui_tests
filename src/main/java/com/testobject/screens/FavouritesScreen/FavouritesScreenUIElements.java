package com.testobject.screens.FavouritesScreen;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;

public class FavouritesScreenUIElements extends AbstractScreen {

    FavouritesScreenUIElements(AppiumDriver driver) {
        super(driver);
    }


    @iOSFindBy(id = "icHeartEmpty")
    @AndroidFindBy(xpath = "//*[@resource-id='com.softhis.tui.stage:id/offer_favourites_heart'][1]")
    WebElement firstRedHeartButton;

    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='icHeartEmpty'])[2]")
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout[1]/android.support.v7.widget.RecyclerView/android.widget.RelativeLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.ImageView")
    WebElement secondRedHeartButton;

    @iOSFindBy(id = "empty set detail label")
    @AndroidFindBy(xpath = "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.support.v4.view.ViewPager/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView")
    WebElement placeholderLabel;

    @iOSFindBy(id = "Ulubione")
    @AndroidFindBy(id = "com.softhis.tui.stage:id/counterTextView")
    WebElement favCounter;

//    @iOSFindBy(id = "tabbar_favorite_count")
    @iOSFindBy(xpath = "//XCUIElementTypeApplication[@name=\"TUI-Stage-Debug\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeTabBar/XCUIElementTypeButton[@name=\"Ulubione\"]/XCUIElementTypeOther/XCUIElementTypeStaticText[1]")
    WebElement favCounterLabel;

    @iOSFindBy(id = "listing_cell_hotel_name")
    @AndroidFindBy(id = "com.softhis.tui.stage:id/nameOfferTv")
    WebElement offerName;

    @iOSFindBy(xpath = "//XCUIElementTypeApplication[@name='TUI-Stage-Debug']/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell")
    @AndroidFindBy(id = "com.softhis.tui.stage:id/offerItemContainer")
    WebElement firstOfferItem;


}
