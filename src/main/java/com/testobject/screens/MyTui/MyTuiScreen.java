package com.testobject.screens.MyTui;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;

public class MyTuiScreen extends AbstractScreen {

    public MyTuiScreen(AppiumDriver driver) {
        super(driver);
    }
    private MyTuiScreenUIElements uiObject = new MyTuiScreenUIElements(driver);

    public Boolean isLoginButtonDisplayed() { return uiObject.loginButton.isDisplayed(); }
}
