package com.testobject.screens.MyTui;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;

public class MyTuiScreenUIElements extends AbstractScreen {

    MyTuiScreenUIElements(AppiumDriver driver) {
        super(driver);
    }

    @AndroidFindBy(id = "com.softhis.tui.stage:id/abLogin")
    @iOSFindBy(xpath = "//XCUIElementTypeButton[@name='ZALOGUJ SIĘ']")
    WebElement loginButton;
}
