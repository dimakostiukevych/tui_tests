package com.testobject.screens.Direction;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

class DirectionScreenUIElements extends AbstractScreen{

    DirectionScreenUIElements(AppiumDriver driver) {
        super(driver);
    }

    //static page elements

    @AndroidFindBy(id = "com.softhis.tui.stage:id/abSelect")
    WebElement submitBtn;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/clear_button")
    WebElement clearAll;

    @AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc='Navigate up']")
    WebElement closeBtn;

    @AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @content-desc='Szukaj']")
    WebElement searchBtn;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/search_src_text")
    WebElement searchField;

    //

    @AndroidFindBy(xpath = "//android.widget.CheckBox[@text='Wszystkie kierunki']")
    WebElement allOfDirections;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/checkBoxItem")
    WebElement oneSearchResultDirection;

    @AndroidFindBy(xpath = "//android.widget.RadioButton")
    WebElement oneSearchResultDirectionForCharter;


}
