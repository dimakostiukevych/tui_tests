package com.testobject.screens.Direction;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;

public class DirectionScreen extends AbstractScreen{

    public DirectionScreen(AppiumDriver driver) {
        super(driver);
    }

    private DirectionScreenUIElements uiObject = new DirectionScreenUIElements(driver);

    //getters

    public String getSearchResult() {

        try{
          String elem = uiObject.oneSearchResultDirection.getText();
          System.out.println(elem);
          return elem;
        }catch (Exception e){
          return notFound;
        }
    }

    //static methods

    public void selectAllOfDirections(){
        uiObject.allOfDirections.click();
        uiObject.submitBtn.click();
    }

    public void clearAllDirections() {
        uiObject.clearAll.click();
        uiObject.submitBtn.click();
    }

    public DirectionScreen findInSearch(String country) {
        uiObject.searchBtn.click();
        uiObject.searchField.sendKeys(country);
        return this;
    }

    public DirectionScreen selectFindedDirection(){
        uiObject.oneSearchResultDirection.click();
        return this;
    }

    public DirectionScreen selectFindedDirectionForCharterTours(){
        uiObject.oneSearchResultDirectionForCharter.click();
        return this;
    }

    public void clickOnSubmit(){

        uiObject.submitBtn.click();
    }

    public void findAndOpenSearchItem(String country){
        this.findInSearch(country);
        this.selectFindedDirection();
        this.clickOnSubmit();
    }
}
