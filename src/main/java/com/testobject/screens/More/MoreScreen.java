package com.testobject.screens.More;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;

public class MoreScreen extends AbstractScreen{

    public MoreScreen(AppiumDriver driver) {
        super(driver);
    }
    private MoreScreenUIElements uiObject = new MoreScreenUIElements(driver);

    public String getApplicationTextViewText() { return uiObject.applicationTextView.getText();}
    public String getSettingsLabelText() {return uiObject.settingsLabel.getText();}
}
