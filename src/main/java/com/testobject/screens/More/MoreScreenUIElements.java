package com.testobject.screens.More;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;

public class MoreScreenUIElements extends AbstractScreen{

    MoreScreenUIElements(AppiumDriver driver) {
        super(driver);
    }

    @AndroidFindBy(xpath = "//android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.widget.TextView[1]")
    WebElement applicationTextView;

    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Ustawienia aplikacji']")
    WebElement settingsLabel;
}
