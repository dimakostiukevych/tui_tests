package com.testobject.screens.Airport;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;

class AirportScreenUIElements extends AbstractScreen{

    AirportScreenUIElements(AppiumDriver driver) {
        super(driver);
    }

    //static page elements

    @iOSFindBy(accessibility = "WYBIERZ")
    @AndroidFindBy(id = "com.softhis.tui.stage:id/abSelect")
    WebElement submitBtn;

    @iOSFindBy(accessibility = "WYCZYŚĆ WSZYSTKO")
    @AndroidFindBy(id = "com.softhis.tui.stage:id/clear_button")
    WebElement clearAll;

    @iOSFindBy(accessibility = "WYBIERZ NAJBLIŻSZE LOTNISKO")
    @AndroidFindBy(id = "com.softhis.tui.stage:id/nearestAirportLabel")
    WebElement nearAirport;

    //airports

    @iOSFindBy(accessibility = "Bydgoszcz")
    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[1]/android.widget.CheckBox")
    WebElement airportBYDGOSZCZ;

    @iOSFindBy(accessibility = "Gdańsk")
    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[2]/android.widget.CheckBox")
    WebElement airportGDANSK;

    @iOSFindBy(accessibility = "Katowice")
    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[3]/android.widget.CheckBox")
    WebElement airportKATOWICE;

    @iOSFindBy(accessibility = "Kraków")
    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[4]/android.widget.CheckBox")
    WebElement airportKRAKOW;

    @iOSFindBy(accessibility = "Poznań")
    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[5]/android.widget.CheckBox")
    WebElement airportPOZNAN;

    @iOSFindBy(accessibility = "Rzeszów")
    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[6]/android.widget.CheckBox")
    WebElement airportRZESZOW;

    @iOSFindBy(accessibility = "Warszawa")
    WebElement airportWarszawa;

    @iOSFindBy(accessibility = "Wrocław")
    WebElement airportWroclaw;


    //airports for Charters Tours

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Bydgoszcz (BZG)']")
    WebElement airportBYDGOSZCZ_CHARTER;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Gdańsk (GDN)']")
    WebElement airportGDANSK_CHARTER;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Katowice (KTW)']")
    WebElement airportKATOWICE_CHARTER;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Kraków (KRK)']")
    WebElement airportKRAKOW_CHARTER;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Poznań (POZ)']")
    WebElement airportPOZNAN_CHARTER;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Rzeszów (RZE)']")
    WebElement airportRZESZOW_CHARTER;


    //Airport checkbox

    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[1]/android.widget.CheckBox")
    WebElement airportBYDGOSZCZ_chcekbox;

    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[2]/android.widget.CheckBox")
    WebElement airportGDANSK_chcekbox;

    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[3]/android.widget.CheckBox")
    WebElement airportKATOWICE_chcekbox;

    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[4]/android.widget.CheckBox")
    WebElement airportKRAKOW_chcekbox;

    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[5]/android.widget.CheckBox")
    WebElement airportPOZNAN_checkbox;

    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[6]/android.widget.CheckBox")
    WebElement airportRZESZOW__checkbox;

}
