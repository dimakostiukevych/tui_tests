package com.testobject.screens.Airport;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;


public class AirportScreen extends AbstractScreen {

    public AirportScreen(AppiumDriver driver) {
        super(driver);
    }

    private AirportScreenUIElements uiObject = new AirportScreenUIElements(driver);

    public void selectNearestAirport(){
        uiObject.nearAirport.click();
    }

    public AirportScreen selectNonOfDirections() {
        uiObject.clearAll.click();
        uiObject.submitBtn.click();
        return this;
    }

    public AirportScreen selectNonOfDirectionsWithoutSubmit() {
        uiObject.clearAll.click();
        return this;
    }

    //selects Airports


    public AirportScreen selectAirportBYDGOSZCZ() {
        uiObject.airportBYDGOSZCZ.click();
        return this;
    }

    public AirportScreen selectAirportGDANSK() {
        uiObject.airportGDANSK.click();
        return this;
    }

    public AirportScreen selectAirportKATOWICE() {
        uiObject.airportKATOWICE.click();
        return this;
    }

    public AirportScreen selectAirportKRAKOW() {
        uiObject.airportKRAKOW.click();
        return this;
    }

    public AirportScreen selectAirportPOZNAN() {
        uiObject.airportPOZNAN.click();
        return this;
    }

    public AirportScreen selectAirportRZESZOW() {
        uiObject.airportRZESZOW.click();
        return this;
    }

    //selects Airports for Charter Tours

    public AirportScreen selectAirportBYDGOSZCZ_CHARTER(){
        uiObject.airportBYDGOSZCZ_CHARTER.click();
        return this;
    }

    public AirportScreen selectAirportGDANSK_CHARTER(){
        uiObject.airportGDANSK_CHARTER.click();
        return this;
    }

    public AirportScreen selectAirportKATOWICE_CHARTER(){
        uiObject.airportKATOWICE_CHARTER.click();
        return this;
    }

    public AirportScreen selectAirportKRAKOW_CHARTER(){
        uiObject.airportKRAKOW_CHARTER.click();
        return this;
    }

    public AirportScreen selectAirportPOZNAN_CHARTER(){
        uiObject.airportPOZNAN_CHARTER.click();
        return this;
    }

    public AirportScreen selectAirportRZESZOW_CHARTER(){
        uiObject.airportRZESZOW_CHARTER.click();
        return this;
    }

    //static methods

    public void clickOnSubmitBtn(){
        uiObject.submitBtn.click();
    }

    //airport checkbox status

    public String getCheckboxStatus_BYDGOSZCZ(){
        return uiObject.airportBYDGOSZCZ_chcekbox.getAttribute("checked");
    }

    public String getCheckboxStatus_GDANSK(){
        return uiObject.airportGDANSK_chcekbox.getAttribute("checked");
    }

    public String getCheckboxStatus_KATOWICE(){
        return uiObject.airportKATOWICE_chcekbox.getAttribute("checked");
    }

    public String getCheckboxStatus_KRAKOW(){
        return uiObject.airportKRAKOW_chcekbox.getAttribute("checked");
    }

    public String getCheckboxStatus_POZNAN(){
        return uiObject.airportPOZNAN_checkbox.getAttribute("checked");
    }

    public String getCheckboxStatus_RZESZOW(){
        return uiObject.airportRZESZOW__checkbox.getAttribute("checked");
    }
}
