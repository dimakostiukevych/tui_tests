package com.testobject.screens.HomePage;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class HomeScreen extends AbstractScreen{

    public HomeScreen(AppiumDriver driver) {
        super(driver);
    }

    private HomeScreenUIElements uiObject = new HomeScreenUIElements(driver);

    //getters

    public String getAmountOfPassangers(){
        return uiObject.amountOfPeople.getText();
    }

    public void getAmountOfViewedItems(){
        try {
            String elem = String.valueOf(uiObject.dotsBar.getSize());
            if (elem.equals("10")) {
                System.out.println("10 elements");
            }
        }catch (NoSuchElementException e){
            System.out.println("There is not 10 elements");
        }
    }

    public void getAmountOfSearchedItems(){
        try{
            String elem = String.valueOf(uiObject.yorSearchedItemsBlock.getSize());
            if (elem.equals("3")){
                System.out.println("3 items");
            }
        }catch (NoSuchElementException e){
            System.out.println("There is not 3 items");
        }
    }

    public String getAirport(){

        try{
          String elem = uiObject.airportType.getText();
          System.out.println(elem);
          return elem;
        }catch (Exception e){
          return notFound;
        }
    }

    public String getDirection() {

        try{
          String elem = uiObject.directionType.getText();
          System.out.println(elem);
          return elem;
        }catch (Exception e){
          return notFound;
        }
    }

    public String getLastWiewedHotelTitle() {

        try {
            String elem = uiObject.lastWiewedHotelTitle.getText();
            System.out.println(elem);
            return elem;
        } catch (Exception e) {
            return notFound;
        }
    }

    public String getVacationDates(){

        try{
          String elem = uiObject.vacationFromTo.getText();
          System.out.println(elem);
          return elem;
        }catch (Exception e){
          return notFound;
        }
    }

    public String getVacationDuration(){

        try{
          String elem = uiObject.duration.getText();
          System.out.println(elem);
          return elem;
        }catch (Exception e){
          return notFound;
        }
    }

    public String getVacationType(){

        try{
          String elem = uiObject.vacationType.getText();
          System.out.println(elem);
          return elem;
        }catch (Exception e){
          return notFound;
        }
    }

    public String getProgressBtnStatus(){

        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOf(uiObject.progressButton));
        return uiObject.progressButton.getAttribute("enabled");
    }

    // get amount of hotels

    public String getHotelsAmount() {

        WebElement submitBtnText = (new WebDriverWait(driver, 20))
                .until(ExpectedConditions.presenceOfElementLocated(By.id("com.softhis.tui.stage:id/tvBtnText")));

        String date = submitBtnText.getText();
        String split[] = date.split(" ");
        String wow1 = split[0];
        String wow2 = split[1];
        String wow3 = split[2];
        System.out.println(wow2);
        return wow2;
    }

    //methods to verify presents of elements

    public boolean isProgressButtonVisible() { return uiObject.progressButton.isDisplayed(); }
    public boolean isRecommendedLabelVisible() { return uiObject.recommendedLabel.isDisplayed(); }
    public boolean verifyLastViewedIsAbsent() throws Exception {
        try {
            driver.findElement(By.xpath("//android.widget.RelativeLayout/android.support.v7.widget.RecyclerView/android.support.v7.widget.LinearLayoutCompat/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView"));
            System.out.println("Element Present");
            return false;

        } catch (NoSuchElementException e) {
            System.out.println("Element absent");
            return true;
        }
    }

    public boolean isAirportBlockIsPresent(){
        return uiObject.airportBlock.isDisplayed();
    }

    public boolean isDirectionBlockIsPresent(){
        return uiObject.directionBlock.isDisplayed();
    }

    //open methods

    public void openAirportSelection(){
        uiObject.airportType.click();
    }

    public void openDirectionSection() {
        uiObject.directionType.click();
    }

    public void openDurationSelection(){
        uiObject.duration.click();
    }

    public void openPassengersSection(){
        uiObject.amountOfPeople.click();
    }

    public void openVacationTypeSelection(){

        uiObject.vacationType.click();
    }

    public void openVacationDatesSection(){
        uiObject.vacationFromTo.click();
    }

    public void clickOnSubmit() throws InterruptedException {
        uiObject.progressButton.click();
        Thread.sleep(4000);
    }

    // TabBar

    public void clickOnContacts() { uiObject.contactsTabBarItem.click(); }
    public Boolean isConctactSelected() { return uiObject.contactsTabBarItem.isSelected(); }

    public void clickOnStart() { uiObject.startTabBarItem.click(); }
    public Boolean isStartSelected() { return uiObject.startTabBarItem.isSelected(); }

    public void clickOnMyTui() { uiObject.myTuiTabBarItem.click(); }
    public Boolean isMyTuiSelected() { return uiObject.myTuiTabBarItem.isSelected(); }

    public void clickOnFavorites() { uiObject.favouritesTabBarItem.click(); }
    public Boolean isFavoritesSelected() { return uiObject.favouritesTabBarItem.isSelected(); }

    public void clickOnMore() { uiObject.moreTabBarItem.click(); }
    public Boolean isMoreSelected() { return uiObject.moreTabBarItem.isSelected(); }


    // Search

    public void clickOnSearch() {
        uiObject.progressButton.click();
    }


    //help methods

    public void openFirstSuggestItem()  {
        uiObject.firstSuggestedElement.click();
    }

    public void openSecondSuggestItem()  {
        uiObject.secondSuggestedElement.click();
    }

    public void openFourthSuggestItem()  {
        uiObject.fourSuggestedElement.click();
    }


    public HomeScreen verticalScroll() throws InterruptedException {

        TouchAction action=new TouchAction(driver);
        action.longPress(uiObject.passengersBlockTitle, Duration.ofMillis(500)).moveTo(489,200).release().perform();
        return this;
    }


    public HomeScreen verticalScrollIfPassengersBlockTitleNotVisible() throws InterruptedException {

        TouchAction action=new TouchAction(driver);
        action.longPress(uiObject.suggestedTitle, Duration.ofMillis(500)).moveTo(489,200).release().perform();
        return this;
    }

    public void doubleVerticalScrollForIOS() throws InterruptedException {
        TouchAction action = new TouchAction(driver);
        action.press(194,350).waitAction(Duration.ofSeconds(5)).moveTo(194,-950).release().perform();
        action.press(194,350).waitAction(Duration.ofSeconds(5)).moveTo(194,-950).release().perform();
    }


    public void clickOnFavourite() {
        MobileElement el1 = (MobileElement) driver.findElementByXPath("//android.widget.HorizontalScrollView/android.widget.LinearLayout/*[2]");
        el1.click();
    }


    // Waiting 30 seconds for an element to be present on the page, checking
    // for its presence once every 5 seconds.

    public HomeScreen waitForFavouritesTabBarItem() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOf(uiObject.favouritesTabBarItem));
        return this;
    }

    public void waitForProgressButton() {
        WebDriverWait wait = new WebDriverWait(driver, 50);
        wait.until(ExpectedConditions.visibilityOf(uiObject.progressButton));
    }

}



