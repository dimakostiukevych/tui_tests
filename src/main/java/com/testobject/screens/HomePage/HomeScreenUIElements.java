package com.testobject.screens.HomePage;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

class HomeScreenUIElements extends AbstractScreen{

    HomeScreenUIElements(AppiumDriver driver) {
        super(driver);
    }

    //page blocks

       ////airport

    @iOSFindBy(accessibility = "LOTNISKO")
    @AndroidFindBy(xpath = "//android.support.v7.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.TextView[1]")
    WebElement airportBlockTitle;

    @iOSFindBy(xpath = "XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]")
    @AndroidFindBy(id = "com.softhis.tui.stage:id/air_port_spinner")
    WebElement airportBlock;

       ///
    @iOSFindBy(xpath = "XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]")
    @AndroidFindBy(xpath = "//android.support.v7.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout[3]/android.widget.TextView[1]")
    WebElement directionBlockTitle;

    @iOSFindBy(xpath = "XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[5]")
    @AndroidFindBy(xpath = "//android.support.v7.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout[5]/android.widget.TextView[1]")
    WebElement passengersBlockTitle;

    //

    @iOSFindBy(xpath = "//XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeStaticText")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[2]")
    WebElement vacationType;

    @iOSFindBy(xpath = "//XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeStaticText")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.TextView[2]")
    WebElement airportType;

       ////direction

    @iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeStaticText")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout[3]/android.widget.TextView[2]")
    WebElement directionType;

    @iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther")
    @AndroidFindBy(id = "com.softhis.tui.stage:id/destination_port_spinner")
    WebElement directionBlock;

    @iOSFindBy(xpath = "//XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeStaticText")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout[4]/android.widget.LinearLayout[1]/android.widget.TextView[2]")
    WebElement duration;

    @iOSFindBy(xpath = "//XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeStaticText")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout[4]/android.widget.LinearLayout[2]/android.widget.TextView[2]")
    WebElement vacationFromTo;

    @iOSFindBy(xpath = "//XCUIElementTypeOther[5]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeStaticText")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.LinearLayout[5]/android.widget.TextView[2]")
    WebElement amountOfPeople;

    @iOSFindBy(id = "search_button")
    @AndroidFindBy(id = "com.softhis.tui.stage:id/progress_button")
    WebElement progressButton;

    //Searched items history

    @AndroidFindBy(xpath = "//android.support.v7.widget.RecyclerView/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.TextView")
    MobileElement searchedItemsBlokTitle;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.widget.FrameLayout[1]/android.widget.LinearLayout")
    WebElement firstSearchedItem;

    @AndroidFindBy(xpath = "//android.support.v7.widget.RecyclerView/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.TextView")
    WebElement firstSearchParamaters;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.widget.FrameLayout[2]/android.widget.LinearLayout")
    WebElement secondSearchedItem;

    @AndroidFindBy(xpath = "//android.support.v7.widget.RecyclerView/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.TextView[2]")
    MobileElement secondSearchParamaters;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/dots_bar")
    WebElement dotsBar;

    @AndroidFindBy(xpath = "//android.support.v7.widget.RecyclerView/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView")
    WebElement yorSearchedItemsBlock;


    //Suggestions

    @iOSFindBy(id= "POLECANE")
    @AndroidFindBy(xpath = "//android.widget.RelativeLayout/android.support.v7.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.TextView")
    WebElement suggestedTitle;

    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[1]/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ImageView")
    WebElement firstSuggestedElement;

    @iOSFindBy(xpath = "//XCUIElementTypeApplication[@name=\"TUI-Stage-Debug\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[3]/XCUIElementTypeOther/XCUIElementTypeOther")
    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[2]/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ImageView")
    WebElement secondSuggestedElement;

    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[3]/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ImageView")
    WebElement thirdSuggestedElement;

    @iOSFindBy(xpath = "//XCUIElementTypeApplication[@name=\"TUI-Stage-Debug\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[4]/XCUIElementTypeOther/XCUIElementTypeOther")
    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[2]/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ImageView")
    WebElement fourSuggestedElement;

    //last viewed section

    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]")
    WebElement lastWiewedHotelTitle;

    // TabBar

    @AndroidFindBy(xpath = " //android.widget.HorizontalScrollView/android.widget.LinearLayout/*[1]")
    @iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Start']")
    WebElement startTabBarItem;

    @AndroidFindBy(xpath = " //android.widget.HorizontalScrollView/android.widget.LinearLayout/*[2]")
    @iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Ulubione']")
    WebElement favouritesTabBarItem;

    @AndroidFindBy(xpath = " //android.widget.HorizontalScrollView/android.widget.LinearLayout/*[3]")
    @iOSFindBy(xpath = "//XCUIElementTypeButton[@name='myTUI']")
    WebElement myTuiTabBarItem;

    @AndroidFindBy(xpath = "//android.widget.HorizontalScrollView/android.widget.LinearLayout/*[4]")
    @iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Kontakt']")
    WebElement contactsTabBarItem;

    @AndroidFindBy(xpath = " //android.widget.HorizontalScrollView/android.widget.LinearLayout/*[5]")
    @iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Więcej']")
    WebElement moreTabBarItem;

    //submit btn text

//    WebElement submitBtnText = (new WebDriverWait(driver, 20))
//            .until(ExpectedConditions.presenceOfElementLocated(By.id("com.softhis.tui.stage:id/tvBtnText")));

    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='POLECANE']")
    WebElement recommendedLabel;
}
