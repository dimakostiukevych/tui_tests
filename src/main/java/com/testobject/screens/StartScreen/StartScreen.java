package com.testobject.screens.StartScreen;

import com.testobject.screens.HomePage.HomeScreen;
import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;

import io.appium.java_client.pagefactory.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;


public class StartScreen extends AbstractScreen {

    public StartScreen(AppiumDriver driver) {
        super(driver);
    }

    private StartScreenUIElements uiObject = new StartScreenUIElements(driver);

    public HomeScreen waitForCapabilityPopupsAndAccept() {
        //uncomment one you need
        return waitForCapabilityPopupsAndAcceptForAndroid();
//        return waitForCapabilityPopupsAndAcceptForiOS();
    }

    public HomeScreen waitForCapabilityPopupsAndAcceptForAndroid(){

        if (uiObject.introStaticText.isDisplayed()) {
            uiObject.introCloseButton.click();
        }

        if (uiObject.popupLocalizationCapabilityforAndroid.isDisplayed()) {
            uiObject.okBtn.click();
        }

        if (uiObject.popupLocalizationCapability.isDisplayed()) {
            uiObject.buttonAllow.click();
        }
        return new HomeScreen(driver);
    }

    public HomeScreen waitForCapabilityPopupsAndAcceptForiOS(){

        if (uiObject.notificationsAlert.isDisplayed()) {
            uiObject.notificationsAlertAllow.click();
        }

        if (uiObject.introStaticText.isDisplayed()) {
            uiObject.introCloseButton.click();
        }

        if (uiObject.changesAlert.isDisplayed()) {
            uiObject.changesAlertClose.click();
        }

        return new HomeScreen(driver);
    }
}
