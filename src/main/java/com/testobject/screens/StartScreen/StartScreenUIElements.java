package com.testobject.screens.StartScreen;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;

class StartScreenUIElements extends AbstractScreen{

    StartScreenUIElements(AppiumDriver driver) {
        super(driver);
    }

    @iOSFindBy(id = "Notifications may include alerts, sounds, and icon badges. These can be configured in Settings.")
    @AndroidFindBy(id = "text")
    WebElement popupNotificationCapability;

    @iOSFindBy(id = "Witaj w aplikacji TUI, Sprawdź, jakie możliwości będą od teraz w zasięgu Twojej ręki:")
    @AndroidFindBy(id= "com.softhis.tui.stage:id/contact_book_tv")
    WebElement introStaticText;

    @iOSFindBy(id = "Pozwoli to zlokalizować najbliższe oddziały i lotniska w okolicy")
    @AndroidFindBy(id = "com.android.packageinstaller:id/dialog_container")
    WebElement popupLocalizationCapability;

    @AndroidFindBy(id= "com.softhis.tui.stage:id/tvDialogTitle")
    WebElement popupLocalizationCapabilityforAndroid;

    @iOSFindBy(id = "ZMIANY W APLIKACJI")
    @AndroidFindBy(id = "com.softhis.tui.stage:id/imgWelcomeScreenClose")
    WebElement popupReleaseNotes;

    @iOSFindBy(id = "Allow")
    @AndroidFindBy(id = "com.android.packageinstaller:id/permission_allow_button")
    WebElement buttonAllow;

    @iOSFindBy(id = "close intro")
    @AndroidFindBy(id = "com.softhis.tui.stage:id/imgWelcomeScreenClose")
    WebElement introCloseButton;

    @iOSFindBy(id = "close")
    @AndroidFindBy(id = "com.softhis.tui.stage:id/imgWelcomeScreenClose")
    WebElement closeButton;

    @iOSFindBy(id = "WEJDŻ DO APLIKACJI")
    @AndroidFindBy(id = "com.softhis.tui.stage:id/rlConfirmOkBtn")
    WebElement openApplication;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/btnPositive")
    WebElement okBtn;

    @iOSFindBy(xpath = "//XCUIElementTypeAlert[@name='“TUI-Stage-Debug” Would Like to Send You Notifications']")
    WebElement notificationsAlert;

    @iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Allow']")
    WebElement notificationsAlertAllow;

    @iOSFindBy(xpath = "//XCUIElementTypeNavigationBar[@name='ZMIANY W APLIKACJI']")
    WebElement changesAlert;

    @iOSFindBy(xpath = "//XCUIElementTypeButton[@name='close']")
    WebElement changesAlertClose;
}
