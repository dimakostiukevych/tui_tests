package com.testobject.screens.VacationType;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;

class VacationTypeScreenUIElements extends AbstractScreen{

    VacationTypeScreenUIElements(AppiumDriver driver) {
        super(driver);
    }

    //static elements

    @AndroidFindBy(id = "com.softhis.tui.stage:id/abSelect")
    WebElement selectBtn;

    @AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc=\"Navigate up\"]")
    WebElement closeBtn;

    //
    @iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell[1]")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.RelativeLayout[1]/android.widget.TextView")
    WebElement airVacation;

    @iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell[2]")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.RelativeLayout[2]/android.widget.TextView")
    WebElement carVacation;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.RelativeLayout[3]/android.widget.TextView")
    WebElement chartersBilets;

    @iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell[3]")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.RelativeLayout[4]/android.widget.TextView")
    WebElement walkingTourVacation;

    @iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell[4]")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.RelativeLayout[5]/android.widget.TextView")
    WebElement skiVacation;



}
