package com.testobject.screens.VacationType;

import com.testobject.screens.HomePage.HomeScreen;
import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;

public class VacationTypeScreen extends AbstractScreen {

    public VacationTypeScreen(AppiumDriver driver) {
        super(driver);
    }

    private VacationTypeScreenUIElements uiObject = new VacationTypeScreenUIElements(driver);

    // static methods

    public void backTo(){
        uiObject.closeBtn.click();
    }

    // methods to select Vacation Tours

    public void selectAirVacation(){
        uiObject.airVacation.click();
    }

    public void selectCarVacation(){
        uiObject.carVacation.click();
    }

    public void selectWalkingTourVacation(){
        uiObject.walkingTourVacation.click();
    }

    public void selectSkiVacation(){
        uiObject.skiVacation.click();
    }

    public void selectChartersTicketsVacation() {
        uiObject.chartersBilets.click();
    }
}
