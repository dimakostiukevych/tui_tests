package com.testobject.screens.VacationDays;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;

class VacationDaysScreenUIElements extends AbstractScreen{

    VacationDaysScreenUIElements(AppiumDriver driver) {
        super(driver);
    }

    //static elements

    @AndroidFindBy(id = "com.softhis.tui.stage:id/abSelect")
    WebElement selectBtn;

    @AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc='Navigate up']")
    WebElement closeBtn;

    //

    @iOSFindBy(accessibility = "3-5 dni")
    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[1]/android.widget.RelativeLayout/android.widget.RadioButton")
    WebElement threeFiveDays;

    @iOSFindBy(accessibility = "6-8 dni")
    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RadioButton")
    WebElement sixEightDays;

    @iOSFindBy(accessibility = "9-12 dni")
    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[3]/android.widget.RelativeLayout/android.widget.RadioButton")
    WebElement nineTwelveDays;

    @iOSFindBy(xpath = "13-15 dni")
    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[4]/android.widget.RelativeLayout/android.widget.RadioButton")
    WebElement thirteenFifteenDays;

    //custom block

    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RadioButton")
    WebElement customAmountOfDays;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/ivMinus")
    WebElement minusDay;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/ivPlus")
    WebElement plusDay;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/tvValue")
    WebElement amountofDays;



}
