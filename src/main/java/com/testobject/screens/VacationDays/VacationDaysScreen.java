package com.testobject.screens.VacationDays;

import com.testobject.screens.HomePage.HomeScreen;
import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;

public class VacationDaysScreen extends AbstractScreen {

    public VacationDaysScreen(AppiumDriver driver) {
        super(driver);
    }

    private VacationDaysScreenUIElements uiObject = new VacationDaysScreenUIElements(driver);


    public void selectThreeFiveDays(){
        uiObject.threeFiveDays.click();
        uiObject.selectBtn.click();
    }

    public void selectSixEightDays(){
        uiObject.sixEightDays.click();
        uiObject.selectBtn.click();
    }

    public void selectNineTwelveDays(){
        uiObject.nineTwelveDays.click();
        uiObject.selectBtn.click();
    }

    public void selectThirteenFifteenDays(){
        uiObject.thirteenFifteenDays.click();
        uiObject.selectBtn.click();
    }

    public void selectCustom9Days() {
        uiObject.plusDay.click();
        uiObject.plusDay.click();
        uiObject.selectBtn.click();
    }
}
