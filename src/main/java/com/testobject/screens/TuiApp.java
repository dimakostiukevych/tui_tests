package com.testobject.screens;

import com.testobject.screens.Airport.AirportScreen;
import com.testobject.screens.ContactScreen.ContactScreen;
import com.testobject.screens.DetailsPages.*;
import com.testobject.screens.Direction.DirectionScreen;
import com.testobject.screens.FavouritesScreen.FavouritesScreen;
import com.testobject.screens.HomePage.HomeScreen;
import com.testobject.screens.More.MoreScreen;
import com.testobject.screens.MyTui.MyTuiScreen;
import com.testobject.screens.Passengers.PassengersScreen;
import com.testobject.screens.StartScreen.StartScreen;
import com.testobject.screens.VacationDates.VacationDates;
import com.testobject.screens.VacationDays.VacationDaysScreen;
import com.testobject.screens.VacationType.VacationTypeScreen;
import io.appium.java_client.AppiumDriver;


public class TuiApp {

    private final AppiumDriver driver;

    public TuiApp(AppiumDriver driver) {
        this.driver = driver;
    }

    public StartScreen startScreen() { return new StartScreen(driver); }
    public HomeScreen homeScreen(){ return new HomeScreen(driver);}
    public FavouritesScreen favouritesScreen() { return new FavouritesScreen(driver);}
    public OfferDetailsScreen offerDetailsScreen() { return new OfferDetailsScreen(driver);}

    //details pages

    public AirportScreen airportScreen() {return new AirportScreen(driver);}
    public DirectionScreen directionsScreen() {return new DirectionScreen(driver);}
    public PassengersScreen passengersScreen() {return new PassengersScreen(driver);}
    public Search searchScreen(){ return new Search(driver);}
    public VacationDates vacationDatesScreen() {return new VacationDates(driver);}
    public VacationDaysScreen vacationDaysScreen() {return new VacationDaysScreen(driver);}
    public VacationTypeScreen vacationTypeScreen() {return new VacationTypeScreen(driver);}

    // TabBar pages
    public ContactScreen contactScreen(){ return new ContactScreen(driver);}
    public MyTuiScreen myTuiScreen(){ return new MyTuiScreen(driver);}
    public MoreScreen moreScreen(){ return new MoreScreen(driver);}

}
