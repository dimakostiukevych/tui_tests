package com.testobject.screens.DetailsPages;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OfferDetailsScreen extends AbstractScreen {

    public OfferDetailsScreen(AppiumDriver driver) { super(driver); }

    private OfferDetailsScreenUIElements uiObject = new OfferDetailsScreenUIElements(driver);

    public String getOfferTitle() {
        return uiObject.offerName.getText();
    }

    public String getOfferRegion() {
        return uiObject.offerCountryRegion.getText();
    }

    public boolean offerIsDisplayed() {
        return uiObject.offerName.isDisplayed() && uiObject.offerCountryRegion.isDisplayed();
    }

    public void waitForToolbarTitle() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOf(uiObject.toolbarTitle));
    }

    public OfferDetailsScreen clickOnToolbarHeart() {
        uiObject.toolbarHeart.click();
        return this;
    }
}
