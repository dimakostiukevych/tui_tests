package com.testobject.screens.DetailsPages;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;


class OfferDetailsScreenUIElements extends AbstractScreen {

    OfferDetailsScreenUIElements(AppiumDriver driver) {
        super(driver);
    }

    @iOSFindBy(id = "offer_details_hotel_name")
    @AndroidFindBy(id = "com.softhis.tui.stage:id/tvDetailNameOffer")
    WebElement offerName;

    @iOSFindBy(id = "offer_details_destination")
    @AndroidFindBy(id = "com.softhis.tui.stage:id/tvCountryRegion")
    WebElement offerCountryRegion;

    @iOSFindBy(id = "offer_details_screen_title")
    @AndroidFindBy(id = "com.softhis.tui.stage:id/toolbar_title")
    WebElement toolbarTitle;

    @iOSFindBy(id = "icHeartOutline")
    @AndroidFindBy(id = "com.softhis.tui.stage:id/menuDetailOfferFavorite")
    WebElement toolbarHeart;

}
