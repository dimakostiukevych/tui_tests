package com.testobject.screens.DetailsPages;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

class SearchScreenUIElements extends AbstractScreen{

    SearchScreenUIElements(AppiumDriver driver) {
        super(driver);
    }

    // static elements

    @AndroidFindBy(id = "com.softhis.tui.stage:id/actionGotoConfigurator")
    WebElement serchEditBtn;

    @AndroidFindBy(accessibility = "Navigate up")
    WebElement backBtn;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/progress_button")
    WebElement submitBtn;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/offerListSortRl")
    WebElement sorting;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/offerListFilterRl")
    WebElement filtering;

    @AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc='Navigate up']")
    WebElement searchProgressBtn;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.support.v7.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[1]")
    WebElement firstHotelInSearchTitle;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/txtToolbarTitle")
    WebElement txtToolbarTitle;

    // frozen search top bar

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[2]/android.widget.TextView")
    WebElement amountOfDaysFromFrozenSearchBar;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[4]/android.widget.TextView[2]")
    WebElement amountOfPassengersFromFrozenSearchBar;

    @AndroidFindBy(xpath = "//android.view.ViewGroup/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView")
    WebElement airportFromFrozenSearchBar;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView")
    WebElement directionFromFrozenSearchBar;

    // page elements

    @AndroidFindBy(xpath = "//android.support.v7.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.ImageView")
    WebElement heart;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/nameOfferTv")
    WebElement firstHotelOnPage;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/toolbar_title")
    WebElement hotelTitle;

    @AndroidFindBy(xpath = "//*[@resource-id='com.softhis.tui.stage:id/priceTextView'][1]")
    WebElement pricePerPerson;

    @AndroidFindBy(xpath = "android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.TextView[1]")
    WebElement priceForAll;

    @AndroidFindBy(xpath = "//com.softhis.tui.stage:id/totalTextView[@text='RAZEM']")
    WebElement priceForAllText;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/txtDateOffer")
    WebElement txtDateOffer;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/imgAirport")
    WebElement airportIcon;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[2]/android.view.ViewGroup/android.widget.TextView[2]")
    WebElement amounOfDaysFromOfferList;

    //page elements // txtFoodOffer

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='All Inclusive']")
    WebElement allInclusiveFoodOffer;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='3 posiłki']")
    WebElement threeMealsFoodOffer;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Śniadanie i obiadokolacja']")
    WebElement breakfastAndDinnerFoodOffer;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Śniadanie']")
    WebElement breakfastFoodOffer;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Bez wyżywienia']")
    WebElement noMealsFoodOffer;

    //sorting popup

    @AndroidFindBy(id = "com.softhis.tui.stage:id/ivCloseSortDialog")
    WebElement closeBtn;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout/android.widget.RelativeLayout/android.support.v7.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.TextView")
    WebElement costUp;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout/android.widget.RelativeLayout/android.support.v7.widget.RecyclerView/android.widget.RelativeLayout[2]/android.widget.TextView")
    WebElement costDown;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout/android.widget.RelativeLayout/android.support.v7.widget.RecyclerView/android.widget.RelativeLayout[3]/android.widget.TextView")
    WebElement tripadvisorRating;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout/android.widget.RelativeLayout/android.support.v7.widget.RecyclerView/android.widget.RelativeLayout[4]/android.widget.TextView")
    WebElement nearestDate;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout/android.widget.RelativeLayout/android.support.v7.widget.RecyclerView/android.widget.RelativeLayout[5]/android.widget.TextView")
    WebElement hotelStandart;

    //search edit screen elements

    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[1]/android.widget.TextView[2]")
    WebElement airport;

    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[2]/android.widget.TextView[2]")
    WebElement direction;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[3]/android.widget.LinearLayout[1]/android.widget.TextView[2]")
    WebElement vacationDays;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.LinearLayout[3]/android.widget.LinearLayout[2]/android.widget.TextView[2]")
    WebElement vacationDates;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[4]/android.widget.TextView[2]")
    WebElement passengers;

    //Filter UI ELEMENTS

    @AndroidFindBy(id = "com.softhis.tui.stage:id/toggleButtonFilter")
    WebElement priceForAllToggle;

    //Amount of filters

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='FILTRUJ (1)']")
    WebElement singleFilter;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='FILTRUJ (2)']")
    WebElement doubleFilter;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='FILTRUJ (3)']")
    WebElement threeFilters;


    //FOOD block

    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.RelativeLayout")
    WebElement foodBlockLayer;

    @AndroidFindBy(xpath = "//android.widget.CheckBox[@text='All Inclusive']")
    WebElement allInclusive;

    @AndroidFindBy(xpath = "//android.widget.CheckBox[@text='3 posiłki']")
    WebElement threeMealsAday;

    @AndroidFindBy(xpath = "//android.widget.CheckBox[@text='Śniadanie i obiadokolacja']")
    WebElement breakfastAndDinner;

    @AndroidFindBy(xpath = "//android.widget.CheckBox[@text='Śniadanie']")
    WebElement breakfast;

    @AndroidFindBy(xpath = "//android.widget.CheckBox[@text='Bez wyżywienia']")
    WebElement noMeals;


    //PRICE for all block

    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.RelativeLayout")
    WebElement priceForAllBlockLayer;

    @AndroidFindBy(xpath = "//android.widget.RadioButton[@text='Dowolna']")
    WebElement anyPricesIncluded;

    @AndroidFindBy(xpath = "//android.widget.RadioButton[@text='Poniżej 4 000 zł']")
    WebElement less4000zl;

    @AndroidFindBy(xpath = "//android.widget.RadioButton[@text='Od 4 000 zł do 6 000 zł']")
    WebElement between4000and6000zl;

    @AndroidFindBy(xpath = "//android.widget.RadioButton[@text='Od 6 000 zł do 10 000 zł']")
    WebElement between6000and10000zl;

    @AndroidFindBy(xpath = "//android.widget.RadioButton[@text='Powyżej 10 000 zł']")
    WebElement more10000zl;

    //SPORT block

    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout[3]/android.widget.RelativeLayout")
    WebElement sportBlockLayer;

    @AndroidFindBy(xpath = "//android.widget.CheckBox[@text='Aquapark']")
    WebElement sportAquapark;

    @AndroidFindBy(xpath = "//android.widget.CheckBox[@text='Zjeżdzalnie dla dzieci']")
    WebElement sportSlidesForChildren;

    @AndroidFindBy(xpath = "//android.widget.CheckBox[@text='Polska animacja']")
    WebElement sportPolandAnimation;

    @AndroidFindBy(xpath = "//android.widget.CheckBox[@text='Brodziki']")
    WebElement sportTrays;

    @AndroidFindBy(xpath = "//android.widget.CheckBox[@text='Siłownia']")
    WebElement sportGym;

    //HOTEL block

    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.RelativeLayout")
    WebElement hotelBlockLayer;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.RadioGroup/android.widget.RadioButton[1]")
    WebElement anyHotelStandart;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.RadioGroup/android.widget.RadioButton[2]")
    WebElement standartFrom5Points;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.RadioGroup/android.widget.RadioButton[3]")
    WebElement standartFrom4Points;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.RadioGroup/android.widget.RadioButton[4]")
    WebElement standartFrom3Points;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.RadioGroup/android.widget.RadioButton[5]")
    WebElement standartFrom2Points;
}
