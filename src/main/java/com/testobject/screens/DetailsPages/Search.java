package com.testobject.screens.DetailsPages;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.apache.commons.lang3.BooleanUtils.or;

public class Search extends AbstractScreen{

    public Search(AppiumDriver driver) {
        super(driver);
    }

    private SearchScreenUIElements uiObject = new SearchScreenUIElements(driver);

    //getters

    public String getAmountOfDays(){ return uiObject.amountOfDaysFromFrozenSearchBar.getText(); }
    public String getAmountOfPassengers(){ return uiObject.amountOfPassengersFromFrozenSearchBar.getText(); }

    public String getHotelTitle(){
        String elem = uiObject.firstHotelInSearchTitle.getText();
        System.out.println(elem);
        return elem;}

    public String getPricePerPerson(){
        String elem = uiObject.pricePerPerson.getText();
        System.out.println(elem);
        return elem;
    }

    public String getPricуForAll(){return uiObject.priceForAll.getText();}

    public String getHotelsAmountInTitle() {
        String date = uiObject.txtToolbarTitle.getText();
        String split[] = date.split(" ");
        String wow1 = split[0];
        String wow2 = split[1];
        System.out.println(wow1);
        return wow1;
    }

    public String getHotelsAmountInSubmitBtn() {
        WebElement submitBtnText = (new WebDriverWait(driver, 20))
                .until(ExpectedConditions.presenceOfElementLocated(By.id("com.softhis.tui.stage:id/tvBtnText")));

        String date = submitBtnText.getText();
        String split[] = date.split(" ");
        String wow1 = split[0];
        String wow2 = split[1];
        String wow3 = split[2];
        System.out.println(wow2);
        return wow2;
    }

    public int getPricePrePerson(){
        String cost = uiObject.pricePerPerson.getText();
        String split[] = cost.split(" ");
        String wow1 = split[0];
        String wow2 = split[1];
        String newS = wow1 + wow2;
        return Integer.parseInt(newS);
    }

    public String getDateOffer(){
        String newStr = uiObject.txtDateOffer.getText();
        String split[] = newStr.split(" ");
        String wow1 = split[0];
        System.out.println("Date is "+wow1);
        return wow1;
    }

    //get prices and compare

    public boolean getPriceAndCompareIfPriceLessThan4000(){

        String costS = uiObject.pricePerPerson.getText();
        String split[] = costS.split(" ");
        String wow1 = split[0];
        String wow2 = split[1];
        String newS = wow1 + wow2;
        System.out.println("Most expensive price is " + newS+". It's less than 4000zl.");

        int cost = Integer.parseInt(newS);
        if (cost <= 4000){
            return true;
            }
        else return false;
    }

    public boolean getPriceAndCompareIfPriceBetween4000and6000() {

        String costS = uiObject.pricePerPerson.getText();
        String split[] = costS.split(" ");
        String wow1 = split[0];
        String wow2 = split[1];
        String newS = wow1 + wow2;
        System.out.println("Most expensive price is " + newS+". It's less than 6000zl.");

        int cost = Integer.parseInt(newS);
        if (cost <= 6000){
            if (cost>=4000){
            }return true;
        }else return false;
    }

    public boolean getPriceAndCompareIfPriceBetween6000and10000() {

        String costS = uiObject.pricePerPerson.getText();
        String split[] = costS.split(" ");
        String wow1 = split[0];
        String wow2 = split[1];
        String newS = wow1 + wow2;
        System.out.println("Most expensive price is " + newS+". It's less than 10000zl.");

        int cost = Integer.parseInt(newS);
        if (cost <= 10000){
            if (cost>=6000){
            }return true;
        }else return false;
    }

    public boolean getPriceAndCompareIfPriceMoreThan10000(){

        String costS = uiObject.pricePerPerson.getText();
        String split[] = costS.split(" ");
        String wow1 = split[0];
        String wow2 = split[1];
        String newS = wow1 + wow2;
        System.out.println("Most expensive price is " + newS+". It's more than 10000zl.");

        int cost = Integer.parseInt(newS);
        if (cost >= 10000){
            return true;
            }
            else return false;
    }

    //get amount of days from offer

    public String getAmountOfDaysForCustomDaysOffer(){

        String amountString = uiObject.amounOfDaysFromOfferList.getText();
        String split[] = amountString.split("()");
        String wow1 = split[1];
        System.out.println("Amount of vacation days on the offer list is "+wow1);
        return wow1;
    }

    public Boolean getAmountOfDaysFor3and5DaysOffer(){

        String amountString = uiObject.amounOfDaysFromOfferList.getText();
        String split[] = amountString.split("()");
        String wow1 = split[1];

        System.out.println("Amount of vacation days on the offer list is "+wow1);
        int cost = Integer.parseInt(wow1);

        if (cost >= 3 & cost <=5){
            System.out.println();
            return true;
        } else return false;
    }

    public Boolean getAmountOfDaysFor6and8DaysOffer(){

        String amountString = uiObject.amounOfDaysFromOfferList.getText();
        String split[] = amountString.split("()");
        String wow1 = split[1];

        System.out.println("Amount of vacation days on the offer list is "+wow1);
        int cost = Integer.parseInt(wow1);

        if (cost >= 6 & cost <=8){
            return true;
        } else return false;
    }


    public Boolean getAmountOfDaysFor9and12DaysOffer(){

        String amountString = uiObject.amounOfDaysFromOfferList.getText();
        String split[] = amountString.split("()");
        String wow1 = split[1];

        System.out.println("Amount of vacation days on the offer list is "+wow1);

        int cost = Integer.parseInt(wow1);
        if (cost >= 9 & cost <=12){
            return true;
        } else return false;
    }

    public Boolean getAmountOfDaysFor13and15DaysOffer(){

        String amountString = uiObject.amounOfDaysFromOfferList.getText();
        String split[] = amountString.split("()");
        String wow1 = split[1];
        String wow2 = split[2];
        String newS = wow1+wow2;
        System.out.println("Amount of vacation days on the offer list is "+newS);

        int cost = Integer.parseInt(newS);

        if (cost >= 13 & cost <=15){
            return true;
        } else return false;
    }

    //

    public String getAirportFromFrozenSearchBar(){
        return uiObject.airportFromFrozenSearchBar.getText();
    }

    public String getDirectionFromFrozenSearchBar(){
        return uiObject.directionFromFrozenSearchBar.getText();
    }

    // static methods

    public void navigateUp(){ uiObject.backBtn.click();}

    public void navigateBack(){driver.navigate().back();}

    public Search clickOnSubmitBtn(){
        uiObject.submitBtn.click();
        return this;
    }

    public Search clickOnFilterringBtn(){
        uiObject.filtering.click();
        return this;
    }
    public Search clickOnSearchEditBtn(){

        uiObject.serchEditBtn.click();
        return this;
    }
    public void clickOnSearchProgressBtn(){ uiObject.searchProgressBtn.click(); }

    public Search clickOnSortingBtn(){
        uiObject.sorting.click();
        return this;
    }


    // changing search parameters

    public void openAirportSection(){ uiObject.airport.click(); }
    public void openDirectionSection(){ uiObject.direction.click(); }
    public void openPassengersSection(){ uiObject.passengers.click(); }
    public void openVacationDaysSection(){ uiObject.vacationDays.click(); }
    public void openVacationDatesSection(){ uiObject.vacationDates.click(); }


    public void openFirstHotelInSearch() {
        if (uiObject.firstHotelOnPage.isDisplayed())
            uiObject.firstHotelOnPage.click();
    }

    public Search openFirstHotelInSearchAndBack() {
        if (uiObject.firstHotelOnPage.isDisplayed())
            uiObject.firstHotelOnPage.click();
        if (uiObject.hotelTitle.isDisplayed()){
            driver.navigate().back();
        }
        return this;
    }

    public boolean verifyFirstElementIsPresent() throws Exception {
        try {
            driver.findElement(By.xpath("//android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout[1]/android.support.v7.widget.RecyclerView/android.widget.RelativeLayout[1]/android.widget.LinearLayout"));
            System.out.println("Element Present");
            return true;

        } catch (NoSuchElementException e) {
            System.out.println("Element absent");
            return false;
        }
    }

    public boolean verifyIfTogglePriceForAllWasEnabled() {
        try{
            uiObject.priceForAllText.isDisplayed();
            return true;
        }catch (NoSuchElementException e){
            System.out.println("Element absent");
            return false;
        }
    }

    public boolean hotelTitleIsVisible(){
        return uiObject.firstHotelInSearchTitle.isDisplayed();
    }

    //sorting

    public Search setSortingByFlyDate() {
        uiObject.nearestDate.click();
        return this;
    }

    public Search setSortingByHotelStandart() {
        uiObject.hotelStandart.click();
        return this;
    }

    public Search setSortingByChapestPrice() {
        uiObject.costDown.click();
        return this;
    }

    public Search setSortingByExpensivePrice(){
        uiObject.costUp.click();
        return this;
    }

    public Search setSortingByTripAdvisorRate() {
        uiObject.tripadvisorRating.click();
        return this;
    }

    //FILTER methods

    public Search turnOnFilterWithPriceForAllToggle() {
        uiObject.priceForAllToggle.click();
        return this;
    }

    /////// FOOD SECTION

    public Search openFoodSection() {
        uiObject.foodBlockLayer.click();
        return this;
    }

    public Search selectAllInclusive(){
        uiObject.allInclusive.click();
        return this;
    }

    public Search selectThreemealsPerDay(){
        uiObject.threeMealsAday.click();
        return this;
    }

    public Search selectBreakfastAndDinner(){
        uiObject.breakfastAndDinner.click();
        return this;
    }

    public Search selectOnlyBreakfast(){
        uiObject.breakfast.click();
        return this;
    }

    public Search selectWithoutMeals(){
        uiObject.noMeals.click();
        return this;
    }

    /////// PRICE FOR ALL SECTION

    public Search openPriceForAllSection(){
        uiObject.priceForAllBlockLayer.click();
        return this;
    }

    public Search selectAnyPricesIncluded() throws InterruptedException {
        uiObject.anyPricesIncluded.click();
        Thread.sleep(3000);
        return this;
    }

    public Search selectLess4000zl() throws InterruptedException {
        uiObject.less4000zl.click();
        Thread.sleep(3000);
        return this;
    }

    public Search selectBetween4000and6000zl() throws InterruptedException {
        uiObject.between4000and6000zl.click();
        Thread.sleep(3000);
        return this;
    }

    public Search selectBetween6000and10000zl() throws InterruptedException {
        uiObject.between6000and10000zl.click();
        Thread.sleep(3000);
        return this;
    }

    public Search selectMore10000zl() throws InterruptedException {
        uiObject.more10000zl.click();
        Thread.sleep(3000);
        return this;
    }

    /////// SPORT SECTION

    public Search openSportSection(){
        uiObject.sportBlockLayer.click();
        return this;
    }

    public Search selectAquaparkSportOffer(){
        uiObject.sportAquapark.click();
        return this;
    }

    public Search selectSlidesForChildren(){
        uiObject.sportSlidesForChildren.click();
        return this;
    }

    public Search selectPolandAnimationOffer(){
        uiObject.sportPolandAnimation.click();
        return this;
    }

    public Search selectTraysOffer(){
        uiObject.sportTrays.click();
        return this;
    }

    public Search selectgymOffer(){
        uiObject.sportGym.click();
        return this;
    }

    /////// HOTEL SECTION

    public Search openHotelSection(){
        uiObject.hotelBlockLayer.click();
        return this;
    }

    public Search selectAnyStandardOfHotel() throws InterruptedException {
        uiObject.anyHotelStandart.click();
        Thread.sleep(3000);
        return this;
    }

    public Search selectStandardFrom5Stars() throws InterruptedException {
        uiObject.standartFrom5Points.click();
        Thread.sleep(3000);
        return this;
    }

    public Search selectStandardFrom4Stars() throws InterruptedException {
        uiObject.standartFrom4Points.click();
        Thread.sleep(3000);
        return this;
    }

    public Search selectStandardFrom3Stars() throws InterruptedException {
        uiObject.standartFrom3Points.click();
        Thread.sleep(3000);
        return this;
    }

    public Search selectStandardFrom2Stars() throws InterruptedException {
        uiObject.standartFrom2Points.click();
        Thread.sleep(3000);
        return this;
    }

    // Amount of filters on search screen

    public boolean singleFilterIsDisplayed(){
        return uiObject.singleFilter.isDisplayed();
    }

    //

    public boolean allInclusiveFoodOfferIsVisibleOnSearchScreenResults(){
        return uiObject.allInclusiveFoodOffer.isDisplayed();
    }

    public boolean threeMealsFoodOfferIsVisibleOnSearchScreenResults(){
        return uiObject.threeMealsFoodOffer.isDisplayed();
    }

    public boolean breakfastAndDinerFoodOfferIsVisibleOnSearchScreenResults(){
        return uiObject.breakfastAndDinnerFoodOffer.isDisplayed();
    }

    public boolean breafastFoodOfferIsVisibleOnSearchScreenResults(){
        return uiObject.breakfastFoodOffer.isDisplayed();
    }

    public boolean noMealsFoodOfferIsVisibleOnSearchScreenResults(){
        return uiObject.noMealsFoodOffer.isDisplayed();
    }

    // methods to verify is elements are visible

    public boolean isAirportIconIsVisibleOnTheOfferListing() {
        return uiObject.airportIcon.isDisplayed();
    }
}
