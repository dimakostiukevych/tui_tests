package com.testobject.screens.Passengers;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;

public class PassengersScreen extends AbstractScreen {

    public PassengersScreen(AppiumDriver driver) {
        super(driver);
    }

    private PassengersScreenUIElements uiObject = new PassengersScreenUIElements(driver);

    // custom issues

    public void clickSumbit(){
        uiObject.selectBtn.click();
    }

    // Grownups

    public PassengersScreen addGrownup(){
        uiObject.plusGrownups.click();
        return this;
    }

    public PassengersScreen reduceGrownups(){
        uiObject.minusGrownups.click();
        return this;

    }

    public PassengersScreen addTwoGrownups(){
        this.addGrownup();
        this.addGrownup();
        return this;
    }

    // Children


    public void addChildren(){
        uiObject.plusChildren.click();
    }

    public void reduceChildren(){
        uiObject.minusChildren.click();
    }

    private void setValidBirhthdayForChildren(){
        if (uiObject.calendar.isDisplayed()){
            uiObject.calendar.click();
        }
        uiObject.calendarYear2006.click();
        uiObject.calendarMonthJan.click();
        uiObject.calendarDay1Jan.click();
    }

    private void setValidBirthdayForSecondChildren(){
        if (uiObject.calendarForSecondChildren.isDisplayed()){
            uiObject.calendarForSecondChildren.click();
        }
        uiObject.calendarYear2007.click();
        uiObject.calendarMonthFeb.click();
        uiObject.calendarDay2Feb.click();
    }

    public PassengersScreen add1Children(){
        this.addChildren();
        this.setValidBirhthdayForChildren();
        return this;
    }

    // other

    public void add1GrownupAnd2Children() {
        this.addGrownup();
        this.addChildren();
        this.addChildren();
        this.setValidBirhthdayForChildren();
        this.setValidBirthdayForSecondChildren();
        this.clickSumbit();
    }

    //iOS

    public PassengersScreen IOS_setBirthDayforChildren(){
        uiObject.wheelDay.sendKeys("1");
        uiObject.wheelMonth.sendKeys("listopada");
        uiObject.wheelYear.sendKeys("2013");
        return this;
    }
}
