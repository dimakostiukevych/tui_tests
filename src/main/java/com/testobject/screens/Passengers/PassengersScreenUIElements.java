package com.testobject.screens.Passengers;

import com.google.common.util.concurrent.ListenableFutureTask;
import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;

import java.util.List;

class PassengersScreenUIElements extends AbstractScreen{

    PassengersScreenUIElements(AppiumDriver driver) {
        super(driver);
    }

    //static elements

    @iOSFindBy(accessibility = "WYBIERZ")
    @AndroidFindBy(id = "com.softhis.tui.stage:id/abSelect")
    WebElement selectBtn;

    @iOSFindBy(accessibility = "WYCZYŚĆ WSZYSTKO")
    @AndroidFindBy(id = "com.softhis.tui.stage:id/clear_button")
    WebElement clearAllBtn;

    @iOSFindBy(accessibility = "Anuluj")
    @AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc='Navigate up']")
    WebElement closeBtn;

    //grownups

    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='icMinus'])[1]")
    @AndroidFindBy(xpath = "//android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.ImageView[1]")
    WebElement minusGrownups;

    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='icPlus'])[1]")
    @AndroidFindBy(xpath = "//android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.ImageView[2]")
    WebElement plusGrownups;

    @iOSFindBy(xpath = "//XCUIElementTypeOther[1]/XCUIElementTypeStaticText[1]")
    @AndroidFindBy(xpath = "//android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.TextView")
    WebElement amountOfGrownups;

    //children

    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='icMinus'])[2]")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ImageView[1]")
    WebElement minusChildren;

    @iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='icPlus'])[2]")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout/android.widget.ImageView[2]")
    WebElement plusChildren;

    @iOSFindBy(xpath = "//XCUIElementTypeOther[2]/XCUIElementTypeStaticText[1]")
    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView")
    WebElement amountOfChildren;

    //calendar

    @iOSFindBy(accessibility = "DD.MM.RR")
    @AndroidFindBy(id = "com.softhis.tui.stage:id/tvDatePickerValue")
    WebElement calendar;

    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='DD.MM.RR'])[1]")
//    @AndroidFindBy(xpath = "")
    WebElement calendarForFirstChildren;

    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='DD.MM.RR'])[2]")
    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[2]/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]")
    WebElement calendarForSecondChildren;

    @iOSFindBy(xpath = "(//XCUIElementTypeStaticText[@name='DD.MM.RR'])[3]")
    @AndroidFindBy(xpath = "//android.widget.RelativeLayout[3]/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[2]")
    WebElement calendarForThirdChildren;

    @AndroidFindBy(xpath = "//android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.GridView/android.widget.TextView[1]")
    WebElement calendarYear2006;

    @AndroidFindBy(xpath = "//android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.GridView/android.widget.TextView[2]")
    WebElement calendarYear2007;

    @AndroidFindBy(xpath = "//android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.GridView/android.widget.TextView[3]")
    WebElement calendarYear2008;

    @AndroidFindBy(xpath = "//android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.GridView/android.widget.TextView[1]")
    WebElement calendarMonthJan;

    @AndroidFindBy(xpath = "//android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.GridView/android.widget.TextView[2]")
    WebElement calendarMonthFeb;

    @AndroidFindBy(xpath = "//android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.GridView/android.widget.TextView[3]")
    WebElement calendarMonthMar;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout/android.widget.LinearLayout/android.widget.GridView/android.widget.RelativeLayout[7]/android.widget.TextView")
    WebElement calendarDay1Jan;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.GridView/android.widget.RelativeLayout[5]/android.widget.TextView")
    WebElement calendarDay2Feb;

    @iOSFindBy(xpath = "//XCUIElementTypePickerWheel[1]")
    WebElement wheelDay;

    @iOSFindBy(xpath = "//XCUIElementTypePickerWheel[2]")
    WebElement  wheelMonth;

    @iOSFindBy(xpath = "//XCUIElementTypePickerWheel[3]")
    WebElement wheelYear;



}
