package com.testobject.screens.ContactScreen;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;

public class ContactScreen extends AbstractScreen {

    public ContactScreen(AppiumDriver driver) {
        super(driver);
    }
    private ContactScreenUIElements uiObject = new ContactScreenUIElements(driver);

    public void clickCallNow(){
        uiObject.callButton.click();
    }

    public boolean dialtactsСontainerisDisplayed() {
        return uiObject.dialtactsСontainer.isDisplayed();
    }

    public void clickInfoButton() {
        uiObject.infoButton.click();
    }

    public String popupHeaderText() {
        return uiObject.headerTextView.getText();
    }

    public String firstInfoText() {
        return uiObject.popupTextViewOne.getText();
    }

    public String secondInfoText() {
        return uiObject.popupTextViewTwo.getText();
    }

    public String thirdInfoText() {
        return uiObject.popupTextViewThree.getText();
    }

    public String callButtonText() {
        return uiObject.popupCallButton.getText();
    }

    public String openingHoursOneText() {
        return uiObject.contactDaysOne.getText();
    }

    public String openingHoursTwoText() {
        return uiObject.contactDaysTwo.getText();
    }

    public Boolean isClockImageViewDisplayed() {
        return uiObject.clockImageView.isDisplayed();
    }

    public Boolean isCallNowButtonDisplayed() { return  uiObject.callButton.isDisplayed(); }

    public boolean verifyIfPointersOnMapIsVisible() {
        return uiObject.mapPointer.isDisplayed();
    }
}
