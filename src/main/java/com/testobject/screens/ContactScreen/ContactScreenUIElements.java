package com.testobject.screens.ContactScreen;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;

public class ContactScreenUIElements extends AbstractScreen {

    ContactScreenUIElements(AppiumDriver driver) {
        super(driver);
    }

    @AndroidFindBy(id = "com.softhis.tui.stage:id/call_now_button")
    @iOSFindBy(xpath = "//XCUIElementTypeButton[@name='ZADZWOŃ TERAZ']")
    WebElement callButton;

    @AndroidFindBy(id = "com.android.dialer:id/dialtacts_container")
    WebElement dialtactsСontainer;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/icon_info_gold")
    @iOSFindBy(xpath = "//XCUIElementTypeButton[@name='icInfoGold']")
    WebElement infoButton;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/contact_book_tv")
    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Zarezerwuj swoje beztroskie wakacje lub dowiedz się więcej o naszej ofercie']")
    WebElement headerTextView;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/ivClock")
    @iOSFindBy(xpath = "//XCUIElementTypeImage[not(@name='icClock')]")
    WebElement clockImageView;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/contact_days1")
    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='pn-pt: sb-nd:']")
    WebElement contactDaysOne;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/contact_days2")
    @iOSFindBy(xpath = "//XCUIElementTypeApplication[@name='TUI-Stage-Debug']/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeStaticText[2]")
    WebElement contactDaysTwo;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/tvBtnText")
    @iOSFindBy(xpath = "//XCUIElementTypeButton[@name='ZADZWOŃ TERAZ']")
    WebElement popupCallButton;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.TextView[1]")
    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='7 dni w tygodniu']")
    WebElement popupTextViewOne;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.TextView[2]")
    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Minimum formalności']")
    WebElement popupTextViewTwo;

    @AndroidFindBy(xpath = "//android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.TextView[3]")
    @iOSFindBy(xpath = "//XCUIElementTypeStaticText[@name='Profesjonalni doradcy, pasjonaci turystyki']")
    WebElement popupTextViewThree;

    @AndroidFindBy(xpath = "//android.view.View[@content-desc='Google Map']")
    WebElement mapPointer;
}
