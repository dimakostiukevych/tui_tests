package com.testobject.screens.VacationDates;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.text.SimpleDateFormat;
import java.util.Date;

public class VacationDates extends AbstractScreen{

    public VacationDates(AppiumDriver driver) {
        super(driver);
    }

    private VacationDatesUIElements uiObject = new VacationDatesUIElements(driver);

    //static methods

    public void clickOnSubmit(){
        uiObject.submitBtn.click();
    }

    public void clickOnClearBtn(){
        uiObject.clearBtn.click();
    }

    public void clickOnCloseBtn(){
        uiObject.closeBtn.click();
    }

    //click on arrows

    public VacationDates clickOnRightArrow(){
        uiObject.arrowRight.click();
        return this;
    }

    public VacationDates clickOnLeftArrow(){
        uiObject.arrowLeft.click();
        return this;
    }

    //select days

    public VacationDates selectFirstDayOfTheMonth(){
        uiObject.firstDayOfTheMonth.click();
        return this;
    }

    public VacationDates select28dayOfTheMonth(){
        uiObject.the28DayOfTheMonth.click();
        return this;
    }

    //get vacation dates

    public String getDateFrom(){
        return uiObject.vacationDatesFrom.getText();
    }

    public String getDateTo(){
        return uiObject.vacationDatesFrom.getText();
    }

    public void checkIfTodayDayIsColored(){

        Date dNOW = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        String dateNOW = ft.format(dNOW);
        String split[] = dateNOW.split("-");
        String wow0 = split[0];
        String wow1 = split[1];
        String wow2 = split[2];
        System.out.println("Today day is "+wow2);

        WebElement elem = driver.findElement(By.xpath("//android.widget.TextView[@text='"+wow2+"']"));
        String ccValueColor = elem.getCssValue("color");
        System.out.println(ccValueColor);
    }


}
