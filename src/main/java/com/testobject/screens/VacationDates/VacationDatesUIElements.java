package com.testobject.screens.VacationDates;

import com.testobject.screens.StaticPages.AbstractScreen;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

import java.text.SimpleDateFormat;
import java.util.Date;

class VacationDatesUIElements extends AbstractScreen {

    VacationDatesUIElements(AppiumDriver driver) {
        super(driver);
    }

    //static elements

    @AndroidFindBy(id = "com.softhis.tui.stage:id/abSelect")
    WebElement submitBtn;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/clear_button")
    WebElement clearBtn;

    @AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc='Navigate up']")
    WebElement closeBtn;

    //arrows

    @AndroidFindBy(id = "com.softhis.tui.stage:id/ivArrowRight")
    WebElement arrowRight;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/ivArrowLeft")
    WebElement arrowLeft;

    //

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='1']")
    WebElement firstDayOfTheMonth;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='28']")
    WebElement the28DayOfTheMonth;

    //vacation dates

    @AndroidFindBy(id = "com.softhis.tui.stage:id/period_picker_date_from")
    WebElement vacationDatesFrom;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/period_picker_date_to")
    WebElement vacationDatesTo;

    @AndroidFindBy(id = "com.softhis.tui.stage:id/tvMonthYearName")
    WebElement mounthYearName;
}
