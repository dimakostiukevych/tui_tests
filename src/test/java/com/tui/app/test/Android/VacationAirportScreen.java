package com.tui.app.test.Android;

import com.testobject.screens.Data.allStrings;
import com.tui.app.utility.AbstractTest;
import org.testng.Assert;
import org.testng.annotations.*;

public class VacationAirportScreen extends AbstractTest{

    @Test
    public void selectNearAirport_test() throws Exception {

        //accept all required terms -> open Airport section

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();
        app.homeScreen().openAirportSelection();

        // mark nearest airportScreen ckeck-box -> go to the main screen and check if changes is on place

        app.airportScreen().selectNearestAirport();
        Assert.assertEquals(app.homeScreen().getAirport(), allStrings.KRAKOW);

        app.homeScreen().clickOnSubmit();
        Assert.assertEquals(app.searchScreen().getAirportFromFrozenSearchBar(), allStrings.KRAKOW);
    }

    @Test
    public void selectAirportFromList_test() throws Exception {

        //accept all required terms -> open Airport section

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();
        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportBYDGOSZCZ()
                .clickOnSubmitBtn();

        Assert.assertEquals(app.homeScreen().getAirport(), allStrings.BYDGOSZCZ);
    }

    @Test
    public void selectSeveralAirports_test() throws Exception {


        //accept all required terms -> open Airport section

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();
        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportKRAKOW()
                .selectAirportRZESZOW()
                .clickOnSubmitBtn();

        Assert.assertEquals(app.homeScreen().getAirport(), allStrings.TWO_AIRPORTS);

        app.homeScreen().openAirportSelection();

        Assert.assertEquals(app.airportScreen().getCheckboxStatus_KRAKOW(), allStrings.TRUE);
        Assert.assertEquals(app.airportScreen().getCheckboxStatus_RZESZOW(), allStrings.TRUE);

        app.airportScreen().selectAirportBYDGOSZCZ()
                .clickOnSubmitBtn();

        Assert.assertEquals(app.homeScreen().getAirport(), allStrings.THREE_AIRPORTS);
    }
}
