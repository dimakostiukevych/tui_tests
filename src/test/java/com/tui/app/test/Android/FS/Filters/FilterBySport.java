package com.tui.app.test.Android.FS.Filters;

import com.tui.app.utility.AbstractTest;
import org.testng.Assert;
import org.testng.annotations.*;

public class FilterBySport extends AbstractTest {

    @Test
    public void aquaparkOffer_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportKRAKOW()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openSportSection()
                .selectAquaparkSportOffer()
                .clickOnSubmitBtn();

        Assert.assertTrue(app.searchScreen().singleFilterIsDisplayed());
    }

    @Test
    public void slidesForChildrenOffer_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportGDANSK()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openSportSection()
                .selectSlidesForChildren()
                .clickOnSubmitBtn();

        Assert.assertTrue(app.searchScreen().singleFilterIsDisplayed());
    }

    @Test
    public void sportOffer_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportRZESZOW()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openSportSection()
                .selectPolandAnimationOffer()
                .clickOnSubmitBtn();

        Assert.assertTrue(app.searchScreen().singleFilterIsDisplayed());
    }

    @Test
    public void traysOffer_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportBYDGOSZCZ()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openSportSection()
                .selectTraysOffer()
                .clickOnSubmitBtn();

        Assert.assertTrue(app.searchScreen().singleFilterIsDisplayed());

    }

    @Test
    public void gymOffer_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportKATOWICE()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openSportSection()
                .selectgymOffer()
                .clickOnSubmitBtn();

        Assert.assertTrue(app.searchScreen().singleFilterIsDisplayed());

    }

}
