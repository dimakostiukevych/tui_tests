package com.tui.app.test.Android;

import com.testobject.screens.Data.allStrings;
import com.tui.app.utility.AbstractTest;
import org.testng.Assert;
import org.testng.annotations.*;

public class VacationDaysScreen extends AbstractTest{

    @Test
    public void selectThreeFiveDays_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectAirportBYDGOSZCZ()
                .selectAirportRZESZOW()
                .selectAirportKRAKOW()
                .selectAirportKATOWICE()
                .selectAirportGDANSK()
                .selectAirportPOZNAN()
                .clickOnSubmitBtn();

        app.homeScreen().openDurationSelection();
        app.vacationDaysScreen().selectThreeFiveDays();

        Assert.assertEquals(app.homeScreen().getVacationDuration(), allStrings.THREE_FIVE_DAYS);

        app.homeScreen().clickOnSubmit();

        Assert.assertEquals(app.searchScreen().getAmountOfDays(), allStrings.THREE_FIVE_DAYS);
        Assert.assertTrue(app.searchScreen().getAmountOfDaysFor3and5DaysOffer());

    }

    @Test
    public void selectSixEightDays_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openDurationSelection();
        app.vacationDaysScreen().selectSixEightDays();

        Assert.assertEquals(app.homeScreen().getVacationDuration(), allStrings.SIX_EIGHT_DAYS);

        app.homeScreen().clickOnSubmit();
        Assert.assertEquals(app.searchScreen().getAmountOfDays(), allStrings.SIX_EIGHT_DAYS);
        Assert.assertTrue(app.searchScreen().getAmountOfDaysFor6and8DaysOffer());

    }

    @Test
    public void selectNineTwelveDays_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openDurationSelection();
        app.vacationDaysScreen().selectNineTwelveDays();

        Assert.assertEquals(app.homeScreen().getVacationDuration(), allStrings.NINE_TWELVE_DAYS);

        app.homeScreen().clickOnSubmit();
        Assert.assertEquals(app.searchScreen().getAmountOfDays(), allStrings.NINE_TWELVE_DAYS);
        Assert.assertTrue(app.searchScreen().getAmountOfDaysFor9and12DaysOffer());

    }

    @Test
    public void selectThirteenFifteenDays_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();
        app.homeScreen().openDurationSelection();
        app.vacationDaysScreen().selectThirteenFifteenDays();

        Assert.assertEquals(app.homeScreen().getVacationDuration(), allStrings.THIRTEEN_FIFTEEN_DAYS);

        app.homeScreen().clickOnSubmit();

        Assert.assertEquals(app.searchScreen().getAmountOfDays(), allStrings.THIRTEEN_FIFTEEN_DAYS);
        Assert.assertTrue(app.searchScreen().getAmountOfDaysFor13and15DaysOffer());

    }


    @Test
    public void selectCustomAmountOfDays_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();
        app.homeScreen().openDurationSelection();
        app.vacationDaysScreen().selectCustom9Days();

        Assert.assertEquals(app.homeScreen().getVacationDuration(), allStrings.CUSTOM_9_DAYS);

        app.homeScreen().clickOnSubmit();

        Assert.assertEquals(app.searchScreen().getAmountOfDays(), allStrings.CUSTOM_9_DAYS);
        Assert.assertEquals(app.searchScreen().getAmountOfDaysForCustomDaysOffer(), allStrings.NINE);

    }

}
