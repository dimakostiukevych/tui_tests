package com.tui.app.test.Android.FS.Filters;

import com.testobject.screens.Data.allStrings;
import com.tui.app.utility.AbstractTest;
import org.testng.Assert;
import org.testng.annotations.*;

public class FilterByPriceForAll extends AbstractTest {

    @Test
    public void anyPricesIncludedOffer_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportKRAKOW()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openPriceForAllSection()
                .selectAnyPricesIncluded()
                .clickOnSubmitBtn();

        Assert.assertTrue(app.searchScreen().singleFilterIsDisplayed());
    }

    @Test
    public void less4000zlOffer_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportKATOWICE()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openPriceForAllSection()
                .selectLess4000zl()
                .clickOnSubmitBtn();

        Assert.assertTrue(app.searchScreen().singleFilterIsDisplayed());
    }

    @Test
    public void between4000and6000zlOffer_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportKATOWICE()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openPriceForAllSection()
                .selectBetween4000and6000zl()
                .clickOnSubmitBtn();

        Assert.assertTrue(app.searchScreen().singleFilterIsDisplayed());
    }

    @Test
    public void between6000and10000zlOffer_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportGDANSK()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openPriceForAllSection()
                .selectBetween6000and10000zl()
                .clickOnSubmitBtn();

        Assert.assertTrue(app.searchScreen().singleFilterIsDisplayed());
    }

    @Test
    public void more10000zlOffer_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportRZESZOW()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openPriceForAllSection()
                .selectMore10000zl()
                .clickOnSubmitBtn();

        Assert.assertTrue(app.searchScreen().singleFilterIsDisplayed());
    }

}
