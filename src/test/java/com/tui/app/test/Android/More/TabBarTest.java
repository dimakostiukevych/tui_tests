package com.tui.app.test.Android.More;

import com.testobject.screens.Data.allStrings;
import com.tui.app.utility.AbstractTest;
import org.testng.Assert;
import org.testng.annotations.*;

public class TabBarTest extends AbstractTest {

    @Test
    public void TabBarItemsTest_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        Assert.assertTrue(app.homeScreen().isStartSelected());
        Assert.assertTrue(app.homeScreen().isProgressButtonVisible());

        app.homeScreen().clickOnFavorites();
        Assert.assertTrue(app.homeScreen().isFavoritesSelected());

        app.homeScreen().clickOnMyTui();
        Assert.assertTrue(app.homeScreen().isMyTuiSelected());
        Assert.assertTrue(app.myTuiScreen().isLoginButtonDisplayed());

        app.homeScreen().clickOnContacts();
        Assert.assertTrue(app.homeScreen().isConctactSelected());
        Assert.assertTrue(app.contactScreen().isCallNowButtonDisplayed());

        app.homeScreen().clickOnMore();
        Assert.assertTrue(app.homeScreen().isMoreSelected());
        Assert.assertEquals(app.moreScreen().getApplicationTextViewText(), allStrings.APPLICATION);


    }
}