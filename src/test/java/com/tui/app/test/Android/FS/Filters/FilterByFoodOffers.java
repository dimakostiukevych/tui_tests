package com.tui.app.test.Android.FS.Filters;
import com.testobject.screens.Data.allStrings;
import com.tui.app.utility.AbstractTest;
import org.testng.Assert;
import org.testng.annotations.*;

public class FilterByFoodOffers extends AbstractTest{

    @Test
    public void allInclusiveFoodOffer_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportKRAKOW()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openFoodSection()
                .selectAllInclusive()
                .clickOnSubmitBtn();

        Assert.assertEquals(String.valueOf(app.searchScreen().singleFilterIsDisplayed()), allStrings.TRUE);
        Assert.assertEquals(String.valueOf(app.searchScreen().allInclusiveFoodOfferIsVisibleOnSearchScreenResults()), allStrings.TRUE);

    }

    @Test
    public void threeMealsFoodOffer_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportGDANSK()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openFoodSection()
                .selectThreemealsPerDay()
                .clickOnSubmitBtn();

        Assert.assertEquals(String.valueOf(app.searchScreen().singleFilterIsDisplayed()), allStrings.TRUE);
        Assert.assertEquals(String.valueOf(app.searchScreen().threeMealsFoodOfferIsVisibleOnSearchScreenResults()), allStrings.TRUE);
    }

    @Test
    public void breakfastAndDinnerFoodOffer_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportRZESZOW()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openFoodSection()
                .selectBreakfastAndDinner()
                .clickOnSubmitBtn();

        Assert.assertEquals(String.valueOf(app.searchScreen().singleFilterIsDisplayed()), allStrings.TRUE);
        Assert.assertEquals(String.valueOf(app.searchScreen().breakfastAndDinerFoodOfferIsVisibleOnSearchScreenResults()), allStrings.TRUE);
    }

    @Test
    public void breakfastFoodOffer_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportPOZNAN()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openFoodSection()
                .selectOnlyBreakfast()
                .clickOnSubmitBtn();

        Assert.assertEquals(String.valueOf(app.searchScreen().singleFilterIsDisplayed()), allStrings.TRUE);
        Assert.assertEquals(String.valueOf(app.searchScreen().breafastFoodOfferIsVisibleOnSearchScreenResults()), allStrings.TRUE);
    }

    @Test
    public void noMealsFoodOffer_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportPOZNAN()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openFoodSection()
                .selectWithoutMeals()
                .clickOnSubmitBtn();

        Assert.assertEquals(String.valueOf(app.searchScreen().singleFilterIsDisplayed()), allStrings.TRUE);
        Assert.assertEquals(String.valueOf(app.searchScreen().noMealsFoodOfferIsVisibleOnSearchScreenResults()), allStrings.TRUE);
    }

}
