package com.tui.app.test.Android;

import com.testobject.screens.Data.allStrings;
import com.tui.app.utility.AbstractTest;
import org.testng.Assert;
import org.testng.annotations.*;

public class VacationDirectionScreen extends AbstractTest{

    @Test
    public void chooseAllDirections() throws Exception {

        //accept all required terms and open Direction Section

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();
        app.homeScreen().openDirectionSection();

        // mark "All of directions"

        app.directionsScreen().selectAllOfDirections();
        Assert.assertEquals(app.homeScreen().getDirection(), allStrings.ALL_OF_DIRECTIONS);
    }

    @Test
    public void selectAndDeselectAllOfDirection() throws Exception {

        //accept all required terms and open Direction Section

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();
        app.homeScreen().openDirectionSection();

        // mark "All of directions" -> go to the main screen and check if changes is on place

        app.directionsScreen().selectAllOfDirections();
        Assert.assertEquals(app.homeScreen().getDirection(), allStrings.ALL_OF_DIRECTIONS);

        // go to the "Direction" section -> press on Clear btn -> go to the main screen and check if changes is on place

        app.homeScreen().openDirectionSection();
        app.directionsScreen().clearAllDirections();

        Assert.assertEquals(app.homeScreen().getDirection(), allStrings.ANY_OF_DIRECTION);
    }

    @Test
    public void tryToFindDirectionInSearch() throws Exception {

        //accept all required terms and open Direction Section

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();
        app.homeScreen().openDirectionSection();

        //open searchScreen field and -> provide County name

        app.directionsScreen().findInSearch(allStrings.SPAIN);

        Assert.assertEquals(app.directionsScreen().getSearchResult(), allStrings.SPAIN);
    }

    @Test
    public void tryToFindDirectionInSearchByTwoSearchWords() throws Exception {

        //accept all required terms and open Direction Section.

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();
        app.homeScreen().openDirectionSection();

        //open searchScreen field and -> provide County name

        app.directionsScreen().findInSearch(allStrings.BULGARIAN_RIVIERA);

        Assert.assertEquals(app.directionsScreen().getSearchResult(), allStrings.BULGARIAN_RIVIERA);

        app.directionsScreen().selectFindedDirection();
        app.directionsScreen().clickOnSubmit();

        Assert.assertEquals(app.homeScreen().getDirection(), allStrings.BULGARIAN_RIVIERA);


    }
}
