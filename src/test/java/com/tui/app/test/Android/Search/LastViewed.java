package com.tui.app.test.Android.Search;

import com.testobject.screens.Data.allStrings;
import com.tui.app.utility.AbstractTest;

import org.testng.Assert;
import org.testng.annotations.*;

public class LastViewed extends AbstractTest {

    @Test
    public void lastViewedIsPresentedOnHomePage_test() throws Exception {

        //accept all required terms and open Direction Section

        app.startScreen().waitForCapabilityPopupsAndAccept();

        app.homeScreen().openAirportSelection();
        app.airportScreen().selectNonOfDirections();

        app.homeScreen().openDirectionSection();

        //open searchScreen field and -> provide County name

        app.directionsScreen().findAndOpenSearchItem(allStrings.MONTENEGRO);

        app.homeScreen().clickOnSubmit();

        String elem = app.searchScreen().getHotelTitle();

        app.searchScreen().openFirstHotelInSearch();

        app.searchScreen().navigateUp();

        Assert.assertTrue(app.searchScreen().hotelTitleIsVisible());

        app.searchScreen().navigateUp();

        Assert.assertEquals(app.homeScreen().getDirection(), allStrings.MONTENEGRO);

        app.homeScreen().verticalScroll();
        Assert.assertEquals(elem, app.homeScreen().getLastWiewedHotelTitle());
    }

    @Test
    public void lastViewedIsNotPresentedOnHomePage_test() throws Exception {

        //accept all required terms and open Direction Section

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();
        app.airportScreen().selectNonOfDirections();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().verifyFirstElementIsPresent();

        app.searchScreen().navigateUp();

        app.homeScreen().verticalScroll();

        app.homeScreen().verifyLastViewedIsAbsent();
    }

    @Test
    public void lastViewedLimit() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();
        app.airportScreen().selectNonOfDirections();

        app.homeScreen().openDirectionSection();

        //open searchScreen field and -> provide County name -> open 1 Hotel from search and navigateBack to the Home Screen

        app.directionsScreen().findAndOpenSearchItem(allStrings.BULGARIAN_RIVIERA);

        app.homeScreen().clickOnSubmit();

        app.searchScreen().openFirstHotelInSearchAndBack()
                .getHotelTitle();

        app.searchScreen().navigateBack();

        //open searchScreen field and -> provide County name -> open 1 Hotel from search and navigateBack to the Home Screen

        app.homeScreen().openDirectionSection();
        app.directionsScreen().clearAllDirections();
        app.homeScreen().openDirectionSection();

        app.directionsScreen().findAndOpenSearchItem(allStrings.MONTENEGRO);

        app.homeScreen().clickOnSubmit();

        app.searchScreen().openFirstHotelInSearchAndBack()
                .getHotelTitle();
        app.searchScreen().navigateBack();

        //open searchScreen field and -> provide County name -> open 1 Hotel from search and navigateBack to the Home Screen

        app.homeScreen().openDirectionSection();
        app.directionsScreen().clearAllDirections();
        app.homeScreen().openDirectionSection();

        app.directionsScreen().findAndOpenSearchItem(allStrings.EGIPT);

        app.homeScreen().clickOnSubmit();

        app.searchScreen().openFirstHotelInSearchAndBack()
                .getHotelTitle();
        app.searchScreen().navigateBack();

        //open searchScreen field and -> provide County name -> open 1 Hotel from search and navigateBack to the Home Screen

        app.homeScreen().openDirectionSection();
        app.directionsScreen().clearAllDirections();
        app.homeScreen().openDirectionSection();

        app.directionsScreen().findAndOpenSearchItem(allStrings.GREECE);

        app.homeScreen().clickOnSubmit();

        app.searchScreen().openFirstHotelInSearchAndBack()
            .getHotelTitle();
        app.searchScreen().navigateBack();


        //open searchScreen field and -> provide County name -> open 1 Hotel from search and navigateBack to the Home Screen

        app.homeScreen().openDirectionSection();
        app.directionsScreen().clearAllDirections();
        app.homeScreen().openDirectionSection();

        app.directionsScreen().findAndOpenSearchItem(allStrings.SEYCHELLES);

        app.homeScreen().clickOnSubmit();

        app.searchScreen().openFirstHotelInSearchAndBack();

        app.searchScreen().getHotelTitle();
        app.searchScreen().navigateBack();

        //open searchScreen field and -> provide County name -> open 1 Hotel from search and navigateBack to the Home Screen

        app.homeScreen().openDirectionSection();
        app.directionsScreen().clearAllDirections();
        app.homeScreen().openDirectionSection();

        app.directionsScreen().findAndOpenSearchItem(allStrings.BULGARIAN_RIVIERA);

        app.homeScreen().clickOnSubmit();

        app.searchScreen().openFirstHotelInSearchAndBack();

        app.searchScreen().getHotelTitle();
        app.searchScreen().navigateBack();

        //open searchScreen field and -> provide County name -> open 1 Hotel from search and navigateBack to the Home Screen

        app.homeScreen().openDirectionSection();
        app.directionsScreen().clearAllDirections();
        app.homeScreen().openDirectionSection();

        app.directionsScreen().findAndOpenSearchItem(allStrings.KENYA);

        app.homeScreen().clickOnSubmit();

        app.searchScreen().openFirstHotelInSearchAndBack();

        app.searchScreen().getHotelTitle();
        app.searchScreen().navigateBack();

        //open searchScreen field and -> provide County name -> open 1 Hotel from search and navigateBack to the Home Screen

        app.homeScreen().openDirectionSection();
        app.directionsScreen().clearAllDirections();
        app.homeScreen().openDirectionSection();

        app.directionsScreen().findAndOpenSearchItem(allStrings.MADEIRA);

        app.homeScreen().clickOnSubmit();

        app.searchScreen().openFirstHotelInSearchAndBack();

        app.searchScreen().getHotelTitle();
        app.searchScreen().navigateBack();

        //open searchScreen field and -> provide County name -> open 1 Hotel from search and navigateBack to the Home Screen

        app.homeScreen().openDirectionSection();
        app.directionsScreen().clearAllDirections();
        app.homeScreen().openDirectionSection();

        app.directionsScreen().findAndOpenSearchItem(allStrings.CROATIA);

        app.homeScreen().clickOnSubmit();

        app.searchScreen().openFirstHotelInSearchAndBack();

        app.searchScreen().getHotelTitle();
        app.searchScreen().navigateBack();

        //open searchScreen field and -> provide County name -> open 1 Hotel from search and navigateBack to the Home Screen

        app.homeScreen().openDirectionSection();
        app.directionsScreen().clearAllDirections();
        app.homeScreen().openDirectionSection();

        app.directionsScreen().findAndOpenSearchItem(allStrings.EGIPT);

        app.homeScreen().clickOnSubmit();

        app.searchScreen().openFirstHotelInSearchAndBack();

        app.searchScreen().getHotelTitle();
        app.searchScreen().navigateBack();


        //open searchScreen field and -> provide County name -> open 1 Hotel from search and navigateBack to the Home Screen

        app.homeScreen().openDirectionSection();
        app.directionsScreen().clearAllDirections();
        app.homeScreen().openDirectionSection();

        app.directionsScreen().findAndOpenSearchItem(allStrings.GREECE);

        app.homeScreen().clickOnSubmit();

        app.searchScreen().openFirstHotelInSearchAndBack();

        app.searchScreen().getHotelTitle();
        app.searchScreen().navigateBack();

        //

        app.homeScreen().verticalScroll();
        app.homeScreen().getAmountOfViewedItems();

    }

}
