package com.tui.app.test.Android;

import com.testobject.screens.Data.allStrings;
import com.tui.app.utility.AbstractTest;
import org.testng.Assert;
import org.testng.annotations.*;

public class VacationDates extends AbstractTest{

    @Test
    public void chooseDateOfDeparture_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openVacationDatesSection();
        app.vacationDatesScreen().clickOnRightArrow()
                .selectFirstDayOfTheMonth();

        String vacationDatesFrom = app.vacationDatesScreen().getDateFrom();

        app.vacationDatesScreen().clickOnSubmit();
        app.homeScreen().clickOnSubmit();
        app.searchScreen().clickOnSortingBtn()
                .setSortingByFlyDate();

        Assert.assertEquals(app.searchScreen().getDateOffer(), vacationDatesFrom);

    }

    @Test
    public void checkIfClearBtnIsWorksAsExpected_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openVacationDatesSection();
        app.vacationDatesScreen().clickOnRightArrow()
                .selectFirstDayOfTheMonth();

        String vacationDatesFrom = app.vacationDatesScreen().getDateFrom();

        app.vacationDatesScreen().clickOnSubmit();

        app.homeScreen().openVacationDatesSection();

        Assert.assertEquals(app.vacationDatesScreen().getDateFrom(), vacationDatesFrom);

        app.vacationDatesScreen().clickOnClearBtn();

        Assert.assertEquals(app.vacationDatesScreen().getDateFrom(), allStrings.DASH);

    }

    @Test
    public void falseDayCheck_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openVacationDatesSection();

        app.vacationDatesScreen().clickOnRightArrow()
                .selectFirstDayOfTheMonth()
                .clickOnCloseBtn();

        Assert.assertEquals(app.homeScreen().getVacationDates(), allStrings.VACATION_DATES);


    }
}
