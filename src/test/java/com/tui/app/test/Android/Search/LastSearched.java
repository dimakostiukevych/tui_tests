package com.tui.app.test.Android.Search;

import com.testobject.screens.Data.allStrings;
import com.tui.app.utility.AbstractTest;

import org.testng.annotations.*;

public class LastSearched extends AbstractTest{

    @Test
    public void lastThreeSearchedItems_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAccept();

        app.homeScreen().openAirportSelection();
        app.airportScreen().selectNonOfDirections();

        app.homeScreen().openDirectionSection();

        //open searchScreen field and -> provide County name -> open 1 Hotel from search and navigateBack to the Home Screen

        app.directionsScreen().findAndOpenSearchItem(allStrings.GREECE);
        app.homeScreen().clickOnSubmit();

        app.searchScreen().navigateBack();

        //open searchScreen field and -> provide County name -> open 1 Hotel from search and navigateBack to the Home Screen

        app.homeScreen().openDirectionSection();
        app.directionsScreen().clearAllDirections();
        app.homeScreen().openDirectionSection();

        app.directionsScreen().findAndOpenSearchItem(allStrings.MONTENEGRO);
        app.homeScreen().clickOnSubmit();
        app.searchScreen().navigateBack();

        //open searchScreen field and -> provide County name -> open 1 Hotel from search and navigateBack to the Home Screen

        app.homeScreen().openDirectionSection();
        app.directionsScreen().clearAllDirections();
        app.homeScreen().openDirectionSection();

        app.directionsScreen().findAndOpenSearchItem(allStrings.EGIPT);
        app.homeScreen().clickOnSubmit();
        app.searchScreen().navigateBack();

        //

        app.homeScreen().verticalScroll();
        app.homeScreen().getAmountOfSearchedItems();
    }
}
