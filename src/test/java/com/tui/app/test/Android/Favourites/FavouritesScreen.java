package com.tui.app.test.Android.Favourites;

import com.testobject.screens.Data.allStrings;
import com.tui.app.utility.AbstractTest;
import org.testng.Assert;
import org.testng.annotations.*;

public class FavouritesScreen extends AbstractTest {

    @Test
    public void noFavourites_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().waitForFavouritesTabBarItem()
                .clickOnFavorites();

        Assert.assertTrue(app.favouritesScreen().thereIsNoFirstFavouriteOffer());
        Assert.assertEquals(app.favouritesScreen().getPlaceholderText(), allStrings.NO_FAVOURITES_ANDROID);
    }

    @Test
    public void addToFavouritesAndDelete_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().waitForFavouritesTabBarItem()
                .verticalScroll()
                .openSecondSuggestItem();

        app.favouritesScreen().clickFirstRedHeart()
                .navigateBack();

        app.homeScreen().waitForFavouritesTabBarItem();

        Assert.assertEquals(1, app.favouritesScreen().getAmountOfFavourites());

        app.homeScreen().clickOnFavorites();
        app.favouritesScreen().clickFirstRedHeart();

        app.homeScreen().clickOnStart();
        app.homeScreen().clickOnFavorites();

        Assert.assertTrue(app.favouritesScreen().thereIsNoFirstFavouriteOffer());
        Assert.assertEquals(app.favouritesScreen().getPlaceholderText(), allStrings.NO_FAVOURITES_ANDROID);
    }

    @Test
    public void deleteFavouriteOfferFromDetailsScreen_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().clickOnSearch();

        app.favouritesScreen().clickFirstRedHeart()
                .navigateBack();

        app.homeScreen().waitForFavouritesTabBarItem()
                .clickOnFavorites();

        app.favouritesScreen().clickFirstOffer();

        app.offerDetailsScreen().clickOnToolbarHeart()
                .navigateBack();

        Assert.assertTrue(app.favouritesScreen().thereIsNoFirstFavouriteOffer());
        Assert.assertEquals(app.favouritesScreen().getPlaceholderText(), allStrings.NO_FAVOURITES_ANDROID);
    }

    @Test
    public void detailsOfFavouriteOffer_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().verticalScroll().
                openSecondSuggestItem();

        app.favouritesScreen().clickFirstRedHeart()
                .navigateBack();

        app.homeScreen().waitForFavouritesTabBarItem()
                .clickOnFavorites();

        String offerTitle = app.favouritesScreen().getOfferTitle();

        app.favouritesScreen().clickFirstOffer();
        app.offerDetailsScreen().waitForToolbarTitle();

        Assert.assertTrue(app.offerDetailsScreen().offerIsDisplayed());
        Assert.assertEquals(offerTitle, app.offerDetailsScreen().getOfferTitle());
    }

    @Test
    public void favouriteCounter_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().verticalScroll()
                .openSecondSuggestItem();

        app.favouritesScreen().clickFirstRedHeart()
                .navigateBack();

        app.homeScreen().waitForFavouritesTabBarItem();

        int amountOfFavourites = app.favouritesScreen().getAmountOfFavourites();
        Assert.assertEquals(1, amountOfFavourites);

        app.homeScreen().verticalScrollIfPassengersBlockTitleNotVisible()
                .openSecondSuggestItem();

        app.favouritesScreen().clickSecondRedHeart()
                .navigateBack();

        app.homeScreen().waitForFavouritesTabBarItem();

        Assert.assertTrue(amountOfFavourites < app.favouritesScreen().getAmountOfFavourites());

        amountOfFavourites = app.favouritesScreen().getAmountOfFavourites();

        app.homeScreen().openSecondSuggestItem();

        app.favouritesScreen().clickFirstRedHeart().
                navigateBack();

        app.homeScreen().waitForFavouritesTabBarItem();

        Assert.assertTrue(amountOfFavourites > app.favouritesScreen().getAmountOfFavourites());
    }

}
