package com.tui.app.test.Android.FS.Sorting;

import com.tui.app.utility.AbstractTest;
import org.testng.Assert;
import org.testng.annotations.*;

public class SortingBy extends AbstractTest {

    @Test
    public void sortingByCheapestPrice_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportKRAKOW()
                .clickOnSubmitBtn();

        String hotels = app.homeScreen().getHotelsAmount();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnSortingBtn()
                .setSortingByChapestPrice();

        Assert.assertEquals(hotels, app.searchScreen().getHotelsAmountInTitle());

    }

    @Test
    public void sortingByExpensivePrice_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportRZESZOW()
                .clickOnSubmitBtn();

        String hotels = app.homeScreen().getHotelsAmount();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnSortingBtn()
                .setSortingByChapestPrice()
                .clickOnSortingBtn()
                .setSortingByExpensivePrice();

        Assert.assertEquals(hotels, app.searchScreen().getHotelsAmountInTitle());

    }

    @Test
    public void sortingByTripAdvisorRate_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportBYDGOSZCZ()
                .clickOnSubmitBtn();

        String hotels = app.homeScreen().getHotelsAmount();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnSortingBtn()
                .setSortingByTripAdvisorRate();

        Assert.assertEquals(hotels, app.searchScreen().getHotelsAmountInTitle());

    }

    @Test
    public void sortingByNearestFlyDate_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportGDANSK()
                .clickOnSubmitBtn();

        String hotels = app.homeScreen().getHotelsAmount();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnSortingBtn()
                .setSortingByFlyDate();

        Assert.assertEquals(hotels, app.searchScreen().getHotelsAmountInTitle());
    }



    @Test
    public void sortingByHotelStandard_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportKATOWICE()
                .clickOnSubmitBtn();

        String hotels = app.homeScreen().getHotelsAmount();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnSortingBtn()
                .setSortingByHotelStandart();

        Assert.assertEquals(hotels, app.searchScreen().getHotelsAmountInTitle());
    }


}
