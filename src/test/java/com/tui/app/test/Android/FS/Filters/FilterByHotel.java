package com.tui.app.test.Android.FS.Filters;

import com.tui.app.utility.AbstractTest;
import org.testng.Assert;
import org.testng.annotations.*;

public class FilterByHotel extends AbstractTest{

    @Test
    public void selectAnyStandardOfHotels_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportKRAKOW()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openHotelSection()
                .selectAnyStandardOfHotel()
                .clickOnSubmitBtn();

        Assert.assertTrue(app.searchScreen().singleFilterIsDisplayed());

    }

    @Test
    public void selectStandardOfHotelsFrom5Stars_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportKRAKOW()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openHotelSection()
                .selectStandardFrom5Stars()
                .clickOnSubmitBtn();

        Assert.assertTrue(app.searchScreen().singleFilterIsDisplayed());
    }

    @Test
    public void selectStandardOfHotelsFrom4Stars_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportKRAKOW()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openHotelSection()
                .selectStandardFrom4Stars()
                .clickOnSubmitBtn();

        Assert.assertTrue(app.searchScreen().singleFilterIsDisplayed());
    }

    @Test
    public void selectStandardOfHotelsFrom3Stars_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportKRAKOW()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openHotelSection()
                .selectStandardFrom3Stars()
                .clickOnSubmitBtn();

        Assert.assertTrue(app.searchScreen().singleFilterIsDisplayed());
    }
}
