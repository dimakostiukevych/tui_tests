package com.tui.app.test.Android.Search;

import com.testobject.screens.Data.allStrings;
import com.tui.app.utility.AbstractTest;
import org.testng.Assert;
import org.testng.annotations.*;

public class FrozenSearch extends AbstractTest{

    @Test
    public void frozenSearch_test() throws Exception {

        //accept all required terms -> scroll to the Suggest section

        app.startScreen().waitForCapabilityPopupsAndAccept();

        app.homeScreen().verticalScroll();
        app.homeScreen().openFirstSuggestItem();

        app.searchScreen().clickOnSearchEditBtn()
                .openPassengersSection();

        //add one Grownup -> go to the Search screen and check if changes is on place

        app.passengersScreen().addGrownup()
                .clickSumbit();

        Assert.assertEquals(app.searchScreen().getAmountOfPassengers(), allStrings.THREE_GROWNUPS_0_CHILDREN);

        app.searchScreen().clickOnSearchProgressBtn();
    }
}
