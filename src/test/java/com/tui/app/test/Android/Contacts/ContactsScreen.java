package com.tui.app.test.Android.Contacts;

import com.testobject.screens.Data.allStrings;
import com.tui.app.utility.AbstractTest;
import org.testng.Assert;
import org.testng.annotations.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContactsScreen extends AbstractTest {

    @Test
    public void contactCallNow_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAccept();

        app.homeScreen().clickOnContacts();

        Assert.assertTrue(app.contactScreen().verifyIfPointersOnMapIsVisible());

        app.contactScreen().clickCallNow();

        Assert.assertTrue(app.contactScreen().dialtactsСontainerisDisplayed());
    }

    @Test
    public void contactShowPopup_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAccept();

        app.homeScreen().clickOnContacts();
        app.contactScreen().clickInfoButton();

        Assert.assertEquals(app.contactScreen().callButtonText(), allStrings.CONTACT_POPUP_CALL_BUTTON);
        Assert.assertEquals(app.contactScreen().popupHeaderText(), allStrings.CONTACT_HEADER_TEXT);
        Assert.assertEquals(app.contactScreen().firstInfoText(), allStrings.CONTACT_POPUP_INFO_ONE);
        Assert.assertEquals(app.contactScreen().secondInfoText(), allStrings.CONTACT_POPUP_INFO_TWO);
        Assert.assertEquals(app.contactScreen().thirdInfoText(), allStrings.CONTACT_POPUP_INFO_THREE_ANDROID);

        Pattern regexOpeningHoursOne = Pattern.compile(allStrings.CONTACT_POPUP_OPENING_HOURS_1_ANDROID);
        Matcher matcherOne = regexOpeningHoursOne.matcher(app.contactScreen().openingHoursOneText());
        Assert.assertTrue(matcherOne.find());

        Pattern regexOpeningHoursTwo = Pattern.compile(allStrings.CONTACT_POPUP_OPENING_HOURS_2_ANDROID);

        Matcher matcherTwo = regexOpeningHoursTwo.matcher(app.contactScreen().openingHoursTwoText());
        Assert.assertTrue(matcherTwo.find());
    }
}
