package com.tui.app.test.Android.FS.Filters;

import com.testobject.screens.Data.allStrings;
import com.tui.app.utility.AbstractTest;
import org.testng.Assert;
import org.testng.annotations.*;

public class FilterByToggle extends AbstractTest {

    @Test
    public void filterWithToggle_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportKRAKOW()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .turnOnFilterWithPriceForAllToggle()
                .clickOnSubmitBtn();

        Assert.assertTrue(app.searchScreen().singleFilterIsDisplayed());
        Assert.assertTrue(app.searchScreen().verifyIfTogglePriceForAllWasEnabled());

    }


}
