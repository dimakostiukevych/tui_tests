package com.tui.app.test.Android.FS;

import com.testobject.screens.Data.allStrings;
import com.tui.app.utility.AbstractTest;
import org.testng.Assert;
import org.testng.annotations.*;

public class Combined extends AbstractTest {

    @Test
    public void sortByPriceGrowUpAndFilterPriceLessThan40000_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openPassengersSection();
        app.passengersScreen().reduceGrownups()
                .clickSumbit();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportBYDGOSZCZ()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openPriceForAllSection()
                .selectLess4000zl()
                .clickOnSubmitBtn()
                .clickOnSortingBtn()
                .setSortingByChapestPrice();

        Assert.assertEquals(String.valueOf(app.searchScreen().getPriceAndCompareIfPriceLessThan4000()), allStrings.TRUE);
    }

    @Test
    public void sortByPriceGrowUpAndFilterPriceBetween4000_6000_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openPassengersSection();
        app.passengersScreen().reduceGrownups()
                .clickSumbit();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportKATOWICE()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openPriceForAllSection()
                .selectBetween4000and6000zl()
                .clickOnSubmitBtn()
                .clickOnSortingBtn()
                .setSortingByChapestPrice();

        Assert.assertEquals(String.valueOf(app.searchScreen().getPriceAndCompareIfPriceBetween4000and6000()), allStrings.TRUE);
    }

    @Test
    public void sortByPriceGrowUpAndFilterPriceBetween6000_10000_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openPassengersSection();
        app.passengersScreen().reduceGrownups()
                .clickSumbit();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportKRAKOW()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openPriceForAllSection()
                .selectBetween6000and10000zl()
                .clickOnSubmitBtn()
                .clickOnSortingBtn()
                .setSortingByChapestPrice();

        Assert.assertEquals(String.valueOf(app.searchScreen().getPriceAndCompareIfPriceBetween6000and10000()), allStrings.TRUE);

    }

    @Test
    public void sortByPriceGrowUpAndFilterPriceMoreThan10000_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openPassengersSection();
        app.passengersScreen().reduceGrownups()
                .clickSumbit();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportGDANSK()
                .clickOnSubmitBtn();

        app.homeScreen().clickOnSubmit();

        app.searchScreen().clickOnFilterringBtn()
                .openPriceForAllSection()
                .selectMore10000zl()
                .clickOnSubmitBtn()
                .clickOnSortingBtn()
                .setSortingByChapestPrice();

        Assert.assertEquals(String.valueOf(app.searchScreen().getPriceAndCompareIfPriceMoreThan10000()), allStrings.TRUE);

    }
}
