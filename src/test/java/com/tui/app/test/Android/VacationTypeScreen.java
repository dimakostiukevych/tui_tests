package com.tui.app.test.Android;

import com.tui.app.utility.AbstractTest;
import org.testng.Assert;
import org.testng.annotations.*;

import static com.testobject.screens.Data.allStrings.*;

public class VacationTypeScreen extends AbstractTest{

    @Test
    public void airVacation() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openVacationTypeSelection();
        app.vacationTypeScreen().selectAirVacation();

        Assert.assertEquals(app.homeScreen().getVacationType(), AIR_VACATION);
        Assert.assertTrue(app.homeScreen().isAirportBlockIsPresent());

        app.homeScreen().clickOnSubmit();

        Assert.assertTrue(app.searchScreen().isAirportIconIsVisibleOnTheOfferListing());
    }

    @Test
    public void carVacation_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openVacationTypeSelection();
        app.vacationTypeScreen().selectCarVacation();

        Assert.assertEquals(app.homeScreen().getVacationType(), CAR_VACATION);
        Assert.assertTrue(app.homeScreen().isDirectionBlockIsPresent());

        app.homeScreen().clickOnSubmit();
        app.searchScreen().clickOnSearchEditBtn();

        Assert.assertEquals(app.homeScreen().getVacationType(), CAR_VACATION);
    }


    @Test
    public void walkingTourVacation_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openVacationTypeSelection();
        app.vacationTypeScreen().selectWalkingTourVacation();

        Assert.assertEquals(app.homeScreen().getVacationType(), WALKING_TOUR_VACATION);
        Assert.assertTrue(app.homeScreen().isDirectionBlockIsPresent());

        app.homeScreen().clickOnSubmit();
        app.searchScreen().clickOnSearchEditBtn();

        Assert.assertEquals(app.homeScreen().getVacationType(), WALKING_TOUR_VACATION);

    }

    @Test
    public void skiTourVacation_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openVacationTypeSelection();
        app.vacationTypeScreen().selectSkiVacation();

        Assert.assertEquals(app.homeScreen().getVacationType(), SKI_VACATION);
        Assert.assertTrue(app.homeScreen().isDirectionBlockIsPresent());

        app.homeScreen().clickOnSubmit();
        app.searchScreen().clickOnSearchEditBtn();

        Assert.assertEquals(app.homeScreen().getVacationType(), SKI_VACATION);
    }

    @Test
    public void chartersTicketsTours_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openVacationTypeSelection();
        app.vacationTypeScreen().selectChartersTicketsVacation();

        Assert.assertEquals(app.homeScreen().getVacationType(), CHARTER_TICKETS_VACATION);
        Assert.assertTrue(app.homeScreen().isAirportBlockIsPresent());
        Assert.assertTrue(app.homeScreen().isDirectionBlockIsPresent());

        Assert.assertEquals(app.homeScreen().getProgressBtnStatus(), FALSE);

        app.homeScreen().openAirportSelection();
        app.airportScreen().selectAirportKRAKOW_CHARTER()
                .clickOnSubmitBtn();

        Assert.assertEquals(app.homeScreen().getProgressBtnStatus(), FALSE);

        app.homeScreen().openDirectionSection();
        app.directionsScreen().findInSearch(BULGARIAN_BURGAS)
                .selectFindedDirectionForCharterTours()
                .clickOnSubmit();

        Assert.assertEquals(app.homeScreen().getProgressBtnStatus(), TRUE);

        app.homeScreen().clickOnSubmit();

        Assert.assertEquals(app.searchScreen().getAirportFromFrozenSearchBar(), KRAKOW);
        Assert.assertEquals(app.searchScreen().getDirectionFromFrozenSearchBar(), BULGARIAN_BURGAS);

        app.searchScreen().clickOnSearchEditBtn();

        Assert.assertEquals(app.homeScreen().getVacationType(), CHARTER_TICKETS_VACATION);

    }
}
