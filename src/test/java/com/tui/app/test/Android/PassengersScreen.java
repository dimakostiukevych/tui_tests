package com.tui.app.test.Android;

import com.testobject.screens.Data.allStrings;
import com.tui.app.utility.AbstractTest;
import org.testng.Assert;
import org.testng.annotations.*;

public class PassengersScreen extends AbstractTest{

    @Test
    public void addTwoGrownups_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportKRAKOW()
                .clickOnSubmitBtn();

        app.homeScreen().openPassengersSection();

        app.passengersScreen().addTwoGrownups()
                .clickSumbit();

        Assert.assertEquals(app.homeScreen().getAmountOfPassangers(), allStrings.FOUR_GROWNUPS_0_CHILDREN);

        app.homeScreen().clickOnSubmit();

        Assert.assertEquals(app.searchScreen().getAmountOfPassengers(), allStrings.FOUR);

    }

    @Test
    public void add1Children_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportKRAKOW()
                .clickOnSubmitBtn();

        app.homeScreen().openPassengersSection();
        app.passengersScreen().add1Children()
                .clickSumbit();

        Assert.assertEquals(app.homeScreen().getAmountOfPassangers(), allStrings.TWO_GROWNUPS_1_CHILDREN);

        app.homeScreen().clickOnSubmit();

        Assert.assertEquals(app.searchScreen().getAmountOfPassengers(), allStrings.TWO_PLUS_ONE);
    }

    @Test
    public void add1GrownupAnd2Children_test() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForAndroid();

        app.homeScreen().openAirportSelection();

        app.airportScreen().selectNonOfDirectionsWithoutSubmit()
                .selectAirportKRAKOW()
                .clickOnSubmitBtn();

        app.homeScreen().openPassengersSection();
        app.passengersScreen().add1GrownupAnd2Children();

        Assert.assertEquals(app.homeScreen().getAmountOfPassangers(), allStrings.THREE_GROWNUPS_2_CHILDREN);

        app.homeScreen().clickOnSubmit();

        Assert.assertEquals(app.searchScreen().getAmountOfPassengers(), allStrings.THREE_PLUS_TWO);
    }

}
