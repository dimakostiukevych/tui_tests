package com.tui.app.test.iOS.Contacts;

import com.testobject.screens.Data.allStrings;
import com.tui.app.utility.AbstractTest;
import org.testng.Assert;
import org.testng.annotations.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContactsScreen extends AbstractTest {


    @Test
    public void ContactCallNow() throws Exception { //works on device, not simulator

        app.startScreen().waitForCapabilityPopupsAndAcceptForiOS();

        app.homeScreen().clickOnContacts();
        app.contactScreen().clickCallNow();

    }

    @Test
    public void ContactShowPopup() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForiOS();

        app.homeScreen().clickOnContacts();
        app.contactScreen().clickInfoButton();

        Assert.assertEquals(app.contactScreen().callButtonText(), allStrings.CONTACT_POPUP_CALL_BUTTON);
        Assert.assertEquals(app.contactScreen().popupHeaderText(), allStrings.CONTACT_HEADER_TEXT);
        Assert.assertEquals(app.contactScreen().firstInfoText(), allStrings.CONTACT_POPUP_INFO_ONE);
        Assert.assertEquals(app.contactScreen().secondInfoText(), allStrings.CONTACT_POPUP_INFO_TWO);
        Assert.assertEquals(app.contactScreen().thirdInfoText(), allStrings.CONTACT_POPUP_INFO_THREE_IOS);
        Assert.assertEquals(app.contactScreen().openingHoursOneText(), allStrings.CONTACT_POPUP_OPENING_HOURS_1_IOS);
        Pattern regexOpeningHoursTwo = Pattern.compile(allStrings.CONTACT_POPUP_OPENING_HOURS_2_IOS);

        Matcher matcherTwo = regexOpeningHoursTwo.matcher(app.contactScreen().openingHoursTwoText());
        Assert.assertTrue(matcherTwo.find());
    }
}
