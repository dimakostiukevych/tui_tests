package com.tui.app.test.iOS.Favourites;

import com.tui.app.utility.AbstractTest;
import org.testng.Assert;
import org.testng.annotations.*;

public class FavouritesScreen extends AbstractTest {

    @Test
    public void noFavouritesIOS() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForiOS();
        app.homeScreen().waitForFavouritesTabBarItem();
        app.homeScreen().clickOnFavorites();

        Assert.assertTrue(app.favouritesScreen().thereIsNoFirstFavouriteOffer());
//        Assert.assertEquals(app.favouritesScreen().getPlaceholderText(), allStrings.NO_FAVOURITES_IOS);
    }

    @Test
    public void addToFavouritesAndDeleteIOS() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForiOS();
        app.homeScreen().waitForFavouritesTabBarItem();
        app.homeScreen().doubleVerticalScrollForIOS();
        app.homeScreen().openFourthSuggestItem();
        app.favouritesScreen().clickFirstRedHeart();
        app.searchScreen().navigateBack();
        app.homeScreen().waitForFavouritesTabBarItem();

        Assert.assertEquals("1", app.favouritesScreen().getAmountStringOfFavouritesForIOS());

        app.homeScreen().clickOnFavorites();
        app.favouritesScreen().clickFirstRedHeart();
        app.homeScreen().clickOnStart();
        app.homeScreen().clickOnFavorites();

        Assert.assertTrue(app.favouritesScreen().thereIsNoFirstFavouriteOffer());
//        Assert.assertEquals(app.favouritesScreen().getPlaceholderText(), allStrings.NO_FAVOURITES_IOS);
    }

    @Test
    public void deleteFavouriteOfferFromDetailsScreenIOS() throws Exception {
        app.startScreen().waitForCapabilityPopupsAndAcceptForiOS();
        app.homeScreen().clickOnSearch();
        app.favouritesScreen().clickFirstRedHeart();
        app.searchScreen().navigateBack();
        app.homeScreen().waitForFavouritesTabBarItem();
        app.homeScreen().clickOnFavorites();
        app.favouritesScreen().clickFirstOffer();
        app.offerDetailsScreen().clickOnToolbarHeart();
        app.searchScreen().navigateBack();

        Assert.assertTrue(app.favouritesScreen().thereIsNoFirstFavouriteOffer());
//        Assert.assertEquals(app.favouritesScreen().getPlaceholderText(), allStrings.NO_FAVOURITES_IOS);
    }

    @Test
    public void detailsOfFavouriteOfferIOS() throws Exception {
        app.startScreen().waitForCapabilityPopupsAndAcceptForiOS();
        app.homeScreen().doubleVerticalScrollForIOS();
        app.homeScreen().openFourthSuggestItem();
        app.favouritesScreen().clickFirstRedHeart();
        app.searchScreen().navigateBack();
        app.homeScreen().waitForFavouritesTabBarItem();
        app.homeScreen().clickOnFavorites();
        String offerTitle = app.favouritesScreen().getOfferTitle();
        app.favouritesScreen().clickFirstOffer();

        Assert.assertTrue(app.offerDetailsScreen().offerIsDisplayed());
        Assert.assertEquals(offerTitle, app.offerDetailsScreen().getOfferTitle());
        // add region assert when id is ready
        //Assert.assertEquals( , app.offerDetailsScreen().getOfferRegion());
    }

    @Test
    public void favouriteCounterIOS() throws Exception {

        app.startScreen().waitForCapabilityPopupsAndAcceptForiOS();
        app.homeScreen().doubleVerticalScrollForIOS();
        app.homeScreen().openFourthSuggestItem();
        app.favouritesScreen().clickFirstRedHeart();
        app.searchScreen().navigateBack();
        app.homeScreen().waitForFavouritesTabBarItem();

        Assert.assertEquals("1", app.favouritesScreen().getAmountStringOfFavouritesForIOS());

        app.homeScreen().openSecondSuggestItem();
        app.favouritesScreen().clickSecondRedHeart();
        app.searchScreen().navigateBack();
        app.homeScreen().waitForFavouritesTabBarItem();

        Assert.assertEquals("2", app.favouritesScreen().getAmountStringOfFavouritesForIOS());

        app.homeScreen().openSecondSuggestItem();
        app.favouritesScreen().clickFirstRedHeart();
        app.searchScreen().navigateBack();
        app.homeScreen().waitForFavouritesTabBarItem();

        Assert.assertEquals("1", app.favouritesScreen().getAmountStringOfFavouritesForIOS());
    }
}

