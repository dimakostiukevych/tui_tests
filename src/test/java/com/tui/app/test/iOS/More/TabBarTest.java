package com.tui.app.test.iOS.More;

import com.testobject.screens.Data.allStrings;
import com.tui.app.utility.AbstractTest;
import org.testng.Assert;
import org.testng.annotations.*;

public class TabBarTest extends AbstractTest {

    @Test
    public void TabBarItemsTest() throws Exception {
        app.startScreen().waitForCapabilityPopupsAndAcceptForiOS();

        Assert.assertTrue(app.homeScreen().isRecommendedLabelVisible());

        app.homeScreen().clickOnFavorites();
        app.homeScreen().clickOnMyTui();
        Assert.assertTrue(app.myTuiScreen().isLoginButtonDisplayed());
        app.homeScreen().clickOnContacts();
        Assert.assertTrue(app.contactScreen().isCallNowButtonDisplayed());
        app.homeScreen().clickOnMore();
        Assert.assertEquals(app.moreScreen().getSettingsLabelText(), allStrings.SETTINGS);

    }
}