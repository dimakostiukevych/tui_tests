package com.tui.app.utility;

import com.testobject.screens.TuiApp;
import com.tui.app.utility.builder.AppiumDriverBuilder;
import com.tui.app.utility.builder.Configuration;
import io.appium.java_client.AppiumDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;

public class AbstractTest {

    private AppiumDriver driver;
    protected TuiApp app;

    @BeforeMethod
    public void connect() throws MalformedURLException {

        this.driver = AppiumDriverBuilder.forAndroid()
                    .withEndpoint(Configuration.localServerEndpoint())
                    .withCapabilities(Configuration.androidAppCapabilities())
                    .build();

//        this.driver = AppiumDriverBuilder.forIOS()
//                .withEndpoint(Configuration.localServerEndpoint())
//                .withCapabilities(Configuration.iosAppCapabilities())
//                .build();

        app = new TuiApp(driver);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

}
