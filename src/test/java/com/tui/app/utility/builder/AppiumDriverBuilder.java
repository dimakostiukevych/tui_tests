package com.tui.app.utility.builder;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;


public abstract class AppiumDriverBuilder<SELF, DRIVER extends AppiumDriver> {

    protected String apiKey;

    public static IOSDriverBuilder forIOS() {
        return new IOSDriverBuilder();
    }

    public static AndroidDriveBuilder forAndroid() { return new AndroidDriveBuilder(); }

    public static class IOSDriverBuilder extends AppiumDriverBuilder<IOSDriverBuilder, IOSDriver> {

        public IOSDriver build() {
            return new IOSDriver(endpoint, capabilities);
        }
    }

    public static class AndroidDriveBuilder extends AppiumDriverBuilder<AndroidDriveBuilder, AndroidDriver> {

        public AndroidDriver build() {
            return new AndroidDriver(endpoint, capabilities);
        }
    }

    protected URL endpoint;
    protected DesiredCapabilities capabilities;

    public SELF withEndpoint(URL endpoint) {
        this.endpoint = endpoint;

        return (SELF) this;
    }

    public SELF withCapabilities(DesiredCapabilities capabilities) {
        this.capabilities = capabilities;

        return (SELF) this;
    }

    public SELF withApiKey(String apiKey) {
        this.apiKey = apiKey;

        return (SELF) this;
    }

    public abstract DRIVER build();
}
