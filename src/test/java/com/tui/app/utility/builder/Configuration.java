package com.tui.app.utility.builder;

import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;


public class Configuration {

    public static DesiredCapabilities iosAppCapabilities() {
        File appDir = new File(System.getProperty("user.dir"),
                "./app/");
        File app = new File(appDir, "TUI-Client-Stage-Server.ipa");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "10.3.1");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 7");
        capabilities.setCapability(MobileCapabilityType.UDID, "9166558bb9b1b2e8a63f73bc43d0a228738dd27b");
        capabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
        capabilities.setCapability(MobileCapabilityType.FULL_RESET, "true");

        return capabilities;
    }

    public static DesiredCapabilities androidAppCapabilities() {
        File appDir = new File(System.getProperty("user.dir"),
                "./app/");
        File app = new File(appDir, "app-TuiStage-debug.apk");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiAutomator2");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "7.0");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Sony");
        capabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
        capabilities.setCapability(MobileCapabilityType.FULL_RESET, "true");

        return capabilities;
    }


    public static URL localServerEndpoint() throws MalformedURLException {
        return new URL("http://0.0.0.0:4723/wd/hub");
    }

    public static boolean isIOSPlatform = false;
}

